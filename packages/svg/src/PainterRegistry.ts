/**
 * @paper-doll/svg
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { MeshElement } from "@paper-doll/core";
import { ElementPainter } from "./ElementPainter";
import { paintPath } from "./PathPainter";

/**
 * Internal directory of element painters
 */
const elementPainters: Record<string, ElementPainter> = {
    "p": paintPath
}

/**
 * Registers new painter.
 * If there is painter already registered for given element type,
 * it will be replaced with new one
 * @param elementType
 * @param painter
 */

export function RegisterPainter(elementType: string, painter: ElementPainter) {
    elementPainters[elementType] = painter;
}

/**
 * Gets painter for given element
 * @param element
 */
export function GetPainter(element: MeshElement): ElementPainter {
    // TODO: Checks for presence?
    return elementPainters[element.type];
}