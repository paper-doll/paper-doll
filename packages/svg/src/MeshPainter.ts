/**
 * @paper-doll/svg
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Mesh } from "@paper-doll/core";
import { Element, G } from "@svgdotjs/svg.js";
import { GetPainter } from "./PainterRegistry";

/**
 * Converts mesh to SVG
 * @param mesh
 */
export function paintMesh(mesh: Mesh): Element {

    var root = new G();

    for (const element of mesh.elements) {
        const paint = GetPainter(element);
        root.add(paint(mesh, element));
    }
    return root;
}