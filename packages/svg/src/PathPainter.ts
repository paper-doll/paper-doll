/**
 * @paper-doll/svg
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Mesh, MeshElement, PathMeshElement } from "@paper-doll/core";
import { Element, Path, PathArray, PathCommand, LineCommand, CurveCommand } from "@svgdotjs/svg.js";

/**
 * Paints the PathMeshElement
 * @param element
 */
export function paintPath(mesh: Mesh, element: MeshElement): Element {
    if (!element) throw new Error("Element not provided.");
    if (element.type != "p") throw new Error("Element must be of type 'p'");

    // Draw path
    const pathElement = element as PathMeshElement;
    let index = 0;
    const svgSegments = pathElement.segments
        .map<PathCommand>(segment => {
            switch (segment) {
                case "M":
                case "L":
                case "T":
                    return [
                        segment,
                        ...mesh.vertices[element.indices[index++]]
                    ] as LineCommand;
                case "Q":
                case "S":
                    return [
                        segment,
                        ...mesh.vertices[element.indices[index++]],
                        ...mesh.vertices[element.indices[index++]]
                    ] as CurveCommand;
                case "C":
                    return [
                        segment,
                        ...mesh.vertices[element.indices[index++]],
                        ...mesh.vertices[element.indices[index++]],
                        ...mesh.vertices[element.indices[index++]]
                    ] as CurveCommand;
                case "Z":
                    return ["Z"] as LineCommand;
            }
        });

    const path = new Path();
    const pathArray = new PathArray(svgSegments);
    path.plot(pathArray);

    return path;
}
