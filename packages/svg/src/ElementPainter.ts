/**
 * @paper-doll/svg
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Mesh, MeshElement } from "@paper-doll/core";
import { Element } from "@svgdotjs/svg.js";

/**
 * Delegate of method that converts
 * MeshElement to SVGElement
 */
export interface ElementPainter {
    (mesh: Mesh, element: MeshElement): Element;
}
