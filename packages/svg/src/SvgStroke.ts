/**
 * @paper-doll/svg
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

 /**
  * SVG Stroke line caps
  */
export type LineCap = "butt" | "round" | "square";

/**
 * SVG Stroke data
 */
export interface SvgStrokeData {
    /**
     * Color of the stroke. Default black
     */
    color: string;
    /**
     * Width of the stroke. Default 1
     */
    width?: number;
    /**
     * Line cap of the stroke. Default "butt"
     */
    cap?: LineCap,
    /**
     * Dash pattern. Default solid line
     */
    dash?: number[];
    /**
     * Opacity as number 0-1. 0 fully transparent, 1 fully opaque.
     * Fully opaque is default
     */
    opacity? : number;
}

/**
 * Stroke definition.
 */
export type SvgStroke = string | SvgStrokeData;
