/**
 * @paper-doll/svg-import
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

export * from "./Import";