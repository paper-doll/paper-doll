/**
 * @paper-doll/svg
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Mesh, MeshElement, PathMeshElement, PathSegmentType, Vertex } from "@paper-doll/core";
import * as SVG from "@svgdotjs/svg.js";
import "@svgdotjs/svg.topath.js";

/**
 * Converts SVG.Shape to Mesh
 *
 * @param container - SVG container to convert
 *
 * @returns SvgMesh
 *
 * Path, Polyline and Polygon shapes are taken as-is.
 * Other shapes are converted to Path
 */
export function svgToMesh(container: SVG.Element): Mesh {

    const vertices: Vertex[] = [];
    const elements: MeshElement[] = [];

    /**
     * Parses SVG path to vertices and mesh element
     * @param path
     */
    function parsePath(path: SVG.Path): void {

        // Process the segments of path
        const segments: PathSegmentType[] = [];
        const indices: number[] = [];

        // Decide whether path should be closed
        let lastTarget: Vertex = [0, 0];

        function pushVertex(x: number, y: number) {
            indices.push(vertices.length);
            lastTarget = [x,y];
            vertices.push(lastTarget);
        }

        path.array().forEach(segment => {
            switch (segment[0]) {
                case "A":
                case "a":
                    throw new Error("Arc is not supported.");
                case "C":
                    segments.push("C");
                    pushVertex(segment[1], segment[2]);
                    pushVertex(segment[3], segment[4]);
                    pushVertex(segment[5], segment[6]);
                    break;
                case "Q":
                case "S":
                    segments.push(segment[0]);
                    pushVertex(segment[1], segment[2]);
                    pushVertex(segment[3], segment[4]);
                    break;
                case "M":
                case "L":
                case "T":
                    segments.push(segment[0]);
                    pushVertex(segment[1], segment[2]);
                    break;
                case "H":
                    segments.push("L"); // Replace with line
                    pushVertex(segment[1], lastTarget[1]);
                    break;
                case "V":
                    segments.push("L"); // Replace with line
                    pushVertex(lastTarget[0], segment[1]);
                    break;
                case "Z":
                    segments.push("Z");
                    break;
                default:
                    throw new Error("Relative coordinates are not supported (yet).");
            };
        });

        const element: PathMeshElement = {
            type: "p",
            indices,
            segments
        };

        elements.push(element);
    }

    function processElement(element: SVG.Element): void {
        switch (element.type) {
            case "path":
                parsePath(element as unknown as SVG.Path);
                break;
            case "circle":
            case "rect":
                // @ts-ignore
                parsePath(element.toPath() as SVG.Path, start);
                break;
            default:
                processChildElements(element.children());
                break;
        }
    }

    function processChildElements(children: SVG.List<SVG.Element>): void {
        for (let i = 0; i < children.length; ++i) {
            processElement(children[i]);
        }
    }

    processChildElements(container.children());

    return {
        vertices,
        elements
    }
}
