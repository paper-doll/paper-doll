/**
 * @galeanne-thorn/paperdoll
 * Copyright © 2019 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import Flatten from "@flatten-js/core";
import { BinderBase, BoundVertex, Cage, Vertex } from "@paper-doll/core";

/**
 * Transformer using diagonals.
 *
 * This is simple naive transformer not recommended for use
 */
export class MeanValueBinder extends BinderBase {

    /**
     * Cage faces as flatten Polygons
     */
    private baseFaces: Flatten.Polygon[];

    /**
     * Creates new MeanValueTransformer
     * @param cage - Base cage
     */
    public constructor(cage: Cage) {
        super(cage);
        this.baseFaces = cage.faces.map(
            face => this.createPolygon(face.map(vi => cage.vertices[vi]))
        );
    }

    /**
     * For each vertex determines the face of cage it belongs to
     *
     * @param vertices Vertices to determine the face for
     * @returns Array with face indexes for each input vertex
     */
    public assignFaces(vertices: Vertex[]): number[] {
        return vertices.map(v => {
            const p = new Flatten.Point(v[0], v[1]);
            const face = this.baseFaces.findIndex(q => q.contains(p));
            if (face >= 0) {
                return face;
            }
            else {
                // Vertex is outside the cage - Try to assign nearest face
                const distances = this.baseFaces.map(q => q.distanceTo(p)[0]);
                const minDistance = Math.min(...distances);
                const nearestFace = distances.findIndex(d => d === minDistance);
                return nearestFace;
            }
        });
    }

    /**
     * Binds list of vertices to the initial cage
     *
     * @param vertices Vertices to bind to the cage
     * @param faces    Optional array with face indexes the vertices belongs to.
     *                 If not specified, it is calculated using the assignFaces function.
     * @returns Tuple of array of bound vertices withing cage-space coordinates and array of face indexes
     */
    public bindVertices(vertices: Vertex[], faces?: number[]): [BoundVertex[], number[]] {
        // If faces is not provided, calculate one
        const resolvedFaces = faces ?? this.assignFaces(vertices);
        // For each vertex, calculate binding coordinates
        const boundVertices = vertices.map(
            (v, i) => this.getBoundCoordinates(v, resolvedFaces[i])
        );
        return [boundVertices, resolvedFaces];
    }

    /**
     * Calculate mean value coordinates of the vertex in the given face
     * @param v - Vertex
     * @param faceIndex - Face to use
     */
    private getBoundCoordinates(v: Vertex, faceIndex: number): number[] {
        const face = this.baseFaces[faceIndex];
        const faceSize = face.vertices.length;
        const facePrev = faceSize - 1;

        const p = new Flatten.Point(v[0], v[1]);

        const vectors = face.vertices.map(ve => new Flatten.Vector(p, ve));

        const weights = face.vertices.map((pe, i) => {
            // Indexes of previous and next point in face
            const piPrev = (i + facePrev) % faceSize;
            const piNext = (i + 1) % faceSize;
            // Vectors
            const vec = vectors[i];
            if (Flatten.Utils.EQ_0(vec.length)) {
                // If the p == pe, return maximal weight = 1
                return 1;
            }
            const vecPrev = vectors[piPrev];
            const vecNext = vectors[piNext];
            // Angles
            const phiPrev = Flatten.Utils.EQ_0(vecPrev.length) ? 0 : vecPrev.angleTo(vec) / 2;
            const phiNext = Flatten.Utils.EQ_0(vecNext.length) ? 0 : vec.angleTo(vecNext) / 2;
            // Weight
            return (Math.tan(phiPrev) + Math.tan(phiNext)) / vec.length;
        });

        const sum = weights.reduce((s, w) => s += w, 0);

        return weights.map(w => w / sum);
    }

    /**
     * Creates Flatten.Polygon from array of vertices
     * @param vertices - Vertices defining polygon
     */
    private createPolygon(vertices: Vertex[]): Flatten.Polygon {
        const p = new Flatten.Polygon();
        p.addFace(vertices.map(v => new Flatten.Point(v[0], v[1])));
        return p;
    }
}
