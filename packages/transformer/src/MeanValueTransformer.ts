/**
 * @galeanne-thorn/paperdoll
 * Copyright © 2019 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { BoundVertex, Cage, Vertex, TransformerBase } from "@paper-doll/core";

/**
 * Transformer using diagonals.
 *
 * This is simple naive transformer not recommended for use
 */
export class MeanValueTransformer extends TransformerBase {
    /**
     * Converts the cage-space coordinates to real coordinates corresponding to new cage
     *
     * @param boundVertices - Coordinates obtained via bind method
     * @param faces - Face indexes per vertex
     * @returns List of vertices corresponding to the cage set via setCage
     */
    public transformVertices(
        targetCage: Cage,
        boundVertices: BoundVertex[],
        faces: number[]
        ): Vertex[] {
        return boundVertices.map((bv, fi) => {
            // Get cage face the bound vertex belongs to
            const face = targetCage.faces[faces[fi]];
            // For each vertex *v* in face get vectors
            const vectors = face.map(
                (vi, i) => {
                    const v = targetCage.vertices[vi]; // Get cage vertex
                    const n = bv[i]; // Get bound vertex value
                    return [v[0] * n, v[1] * n]; // Multiply vertex by bound vertex value
                });
            // Get sum of weights
            const sum = bv.reduce((s, w) => s += w, 0);
            // Combine vectors
            const point = vectors
                .reduce((r, v) => [r[0] + v[0], r[1] + v[1]], [0,0]);
            // Normalize
            return [point[0] / sum, point[1] / sum] as Vertex;
        });
    }
}
