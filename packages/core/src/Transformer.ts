/**
 * @paper-doll/core
 * Copyright © 2019 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Cage } from "./Cage";
import { BoundVertex, Vertex } from "./Vertex";
import { Mesh } from "./Mesh";
import { BoundMesh } from "./BoundMesh";

/**
 * Transformer interface
 */
export interface Transformer {
    /**
     * Converts the bound vertices in cage-space coordinates
     * to normal [x,y] ordinates corresponding to the target cage.
     *
     * @param targetCage - Target cage
     * @param boundVertices - Bound coordinates
     * @param faces - Cage face indexes per vertex
     * @returns List of vertices corresponding to the cage set via setCage
     */
    transformVertices(
        targetCage: Cage,
        boundVertices: BoundVertex[],
        faces: number[]
        ): Vertex[];

    /**
     * Converts the bound graphics to graphics in [x,y] coordinates
     * corresponding to the target cage.
     *
     * @param targetCage - Target cage
     * @param boundVertices - Bound coordinates
     * @param faces - Face indexes per vertex
     * @returns List of vertices corresponding to the cage set via setCage
     */
    transform(targetCage: Cage, boundGraphics: BoundMesh): Mesh;
}

export abstract class TransformerBase implements Transformer {

 /**
     * Converts the bound vertices in cage-space coordinates
     * to normal [x,y] ordinates corresponding to the target cage.
     *
     * @param targetCage - Target cage
     * @param boundVertices - Bound coordinates
     * @param faces - Cage face indexes per vertex
     * @returns List of vertices corresponding to the cage set via setCage
     */
    public abstract transformVertices(
        targetCage: Cage,
        boundVertices: BoundVertex[],
        faces: number[]
        ): Vertex[];

    /**
     * Converts the bound graphics to graphics in [x,y] coordinates
     * corresponding to the target cage.
     *
     * @param targetCage - Target cage
     * @param boundVertices - Bound coordinates
     * @param faces - Face indexes per vertex
     * @returns List of vertices corresponding to the cage set via setCage
     */
    public transform(targetCage: Cage, boundGraphics: BoundMesh): Mesh {
        const { boundVertices, boundFaces: faces, cageId, ...rest} = boundGraphics;
        return {
            ...rest,
            vertices: this.transformVertices(
                targetCage,
                boundVertices,
                faces
                )
        }
    }
}