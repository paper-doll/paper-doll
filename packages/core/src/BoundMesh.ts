/**
 * @paper-doll/core
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Mesh } from "./Mesh";
import { BoundVertex } from "./Vertex";

/**
 * Graphics that is bound to a cage.
 */
export interface BoundMesh extends Mesh {
    /**
     * Id of the governing cage
     */
    cageId: string;
    /**
     * Vertices in cage space.
     * Must be of same length as vertices array.
     */
    boundVertices: BoundVertex[];
    /**
     * Indices of Cage faces the graphics vertices are bound to.
     * Must be of same length as boundVertices and vertices array.
     */
    boundFaces: number[];
}
