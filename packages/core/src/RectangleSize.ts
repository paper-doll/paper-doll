/**
 * @paper-doll/core
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

export interface RectangleSize {
    /**
     * Width of the area
     */
    width: number,
    /**
     * Height of the area
     */
    height: number
};