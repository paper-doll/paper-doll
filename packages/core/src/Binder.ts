/**
 * @paper-doll/core
 * Copyright © 2019 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { BoundVertex, Vertex } from "./Vertex";
import { Mesh } from "./Mesh";
import { BoundMesh } from "./BoundMesh";
import { Cage } from "./Cage";

/**
 * Binder interface.
 *
 * Assumes one binder per cage
 */
export interface Binder {

    /**
     * For each vertex determines the face of cage it belongs to
     *
     * @param vertices Vertices to determine the face for
     * @returns Array with face indexes for each input vertex
     */
    assignFaces(vertices: Vertex[]): number[]

    /**
     * Binds list of vertices to the initial cage
     *
     * @param vertices Vertices to bind to the cage.
     * @param faces    Optional array with cage face indexes the vertices
     *                 belongs to. If not specified, it is calculated using the
     *                 assignFaces function.
     * @returns Tuple of array of bound vertices withing cage-space coordinates
     *          and array of face indexes.
     */
    bindVertices(vertices: Vertex[], faces?: number[])
        : [BoundVertex[], number[]];

    /**
     * Bids graphics to cage and returns bound graphics
     *
     * @param graphics Graphics to bind to cage.
     * @param faces    Optional array with cage face indexes the vertices
     *                 belongs to. If not specified, it is calculated using the
     *                 assignFaces function.
     * @returns Bound graphics.
     */
    bind(graphics: Mesh, faces?: number[]): BoundMesh;
}

/**
 * Base class for binder
 */
export abstract class BinderBase implements Binder {

    /**
     * Creates new instance if Binder
     *
     * @param cage Cage to bind to
     */
    protected constructor(private cage: Cage) { }

    /**
     * For each vertex determines the face of cage it belongs to
     *
     * @param vertices Vertices to determine the face for
     * @returns Array with face indexes for each input vertex
     */
    public abstract assignFaces(vertices: Vertex[]): number[];

    /**
     * Binds list of vertices to the initial cage
     *
     * @param vertices Vertices to bind to the cage.
     * @param faces    Optional array with cage face indexes the vertices
     *                 belongs to. If not specified, it is calculated using the
     *                 assignFaces function.
     * @returns Tuple of array of bound vertices withing cage-space coordinates
     *          and array of face indexes.
     */
    public abstract bindVertices(vertices: Vertex[], faces?: number[])
        : [BoundVertex[], number[]];

    /**
     * Bids graphics to cage and returns bound graphics
     *
     * @param graphics Graphics to bind to cage.
     * @param faces    Optional array with cage face indexes the vertices
     *                 belongs to. If not specified, it is calculated using the
     *                 assignFaces function.
     * @returns Bound graphics.
     */
    public bind(graphics: Mesh, faces?: number[]): BoundMesh {
        const result = this.bindVertices(graphics.vertices, faces);
        return {
            ...graphics,
            cageId: this.cage.id,
            boundVertices: result[0],
            boundFaces: result[1]
        };
    }


}