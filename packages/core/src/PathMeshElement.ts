/**
 * @paper-doll/core
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { MeshElement } from "./Mesh";

/**
 * Supported SVG path segments
 */
export type PathSegmentType = "M" | "L" | "C" | "S" | "Q" | "T" | "Z";

/**
 * Mesh representing Path
 */
export interface PathMeshElement extends MeshElement {
    /**
     * Type of the mesh
     */
    type: "p";

    /**
     * Path segments
     */
    segments: PathSegmentType[];
}
