/**
 * @paper-doll/core
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

/**
 * 2D vertex
 */
export type Vertex = [x: number, y: number];

/**
 * Array of vertices
 */
export type Vertices = Array<Vertex>;

/**
 * Clones the vertices
 * @param source
 */
export function cloneVertices(source: Vertices): Vertices {
    return source.map(v => [...v]);
}

/**
 * Bound 2D vertex in cage coordinates space
 */
export type BoundVertex = number[];

/**
 * Array of bound vertices
 */
export type BoundVertices = Array<BoundVertex>;
