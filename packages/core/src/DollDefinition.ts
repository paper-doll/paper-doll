/**
 * @paper-doll/core
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { BoundMesh } from "./BoundMesh";
import { Cage } from "./Cage";
import { DollPart } from "./DollPart";
import { MorphTarget } from "./MorphTarget";
import { RectangleSize } from "./RectangleSize";

/**
 * Definition of paper doll
 */
export interface DollDefinition extends RectangleSize {

    /**
     * Id of the doll
     */
    id: string;

    /**
     * Name of the doll
     */
    name: string;

    /**
     * Cages used by the doll
     */
    cages: Record<string, Cage>;

    /**
     * Morph targets used by the doll
     */
    morphs: Record<string, MorphTarget>

    /**
     * Graphics used by the doll
     */
    boundGraphics: Record<string, BoundMesh>

    /**
     * Doll parts
     */
    parts: Record<string, DollPart>
}
