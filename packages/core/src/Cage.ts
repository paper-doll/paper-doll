/**
 * @paper-doll/core
 * Copyright © 2019 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Vertex } from "./Vertex";

/**
 * Face represents closed polygon used as free form transformation boundary.
 *
 * Face must consists of minimum three indices pointing to cage vertices.
 * Technically it is array with 3 or more numbers.
 */
export type Face = {
    0: number,
    1: number,
    2: number
} & number[];

/**
 * Cage.
 *
 * Cage represents the free form transformation boundaries that govern the
 * graphics transformation.
 *
 * One cage can consist of multiple faces. This may be useful to:
 *  * save space when faces have same vertices
 *  * improve the precision and performance of the transformation
 */
export interface Cage {
    /**
     * Id of the cage
     */
    id: string;

    /**
     * Human readable name
     */
    name: string;

    /**
     * Cage vertices
     */
    vertices: Vertex[];

    /**
     * Cage faces
     */
    faces: Face[];
}
