/**
 * @paper-doll/core
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Vertex } from "./Vertex";

/**
 * Key in morph target
 */
export interface MorphTargetKey {
    /**
     * Morph target value this key is for
     */
    value: number;

    /**
     * Cage modifications for this key.
     *
     * Array of modified vertices corresponding to cage indices
     */
    vertices: Vertex[]
}

/**
 * Cage affected by morph target
 */
export interface MorphTargetCage {
    /**
     * Cage Id
     */
    id: string;

    /**
     * Indices affected by the morph
     *
     * Key is cage ID
     * Value is array of indices of affected cage vertices
     */
    indices: number[];

    /**
     * Morph keys for this cage.
     * At least one key needs to exist for the max value of morph target
     */
    keys: Record<number, MorphTargetKey>;
}

/**
 * Morph target
 */
export interface MorphTarget {
    /**
     * Id of the morph target
     */
    id: string;

    /**
     * Human readable name
     */
    name: string;

    /**
     * Min value of the morph target.
     * Must be smaller then max.
     * If undefined, only the range <base; max> is considered.
     */
    min?: number;

    /**
     * Max value of the morph target
     * Must be greater than min and greater than base.
     */
    max: number;

    /**
     * Value of the morph target that corresponds with unmodified cage.
     * Must lie in <min; max) range
     */
    base: number;

    /**
     * Cages affected by this morph
     */
    cages: Record<string, MorphTargetCage>;
}

