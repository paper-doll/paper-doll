/**
 * @paper-doll/core
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Vertex } from "./Vertex";

/**
 * Element of mesh
 */
export interface MeshElement {
    /**
     * Type of mesh element
     */
    type: string;

    /**
     * Array of indices to mesh vertices that compose the element
     */
    indices: number[];
}

/**
 * Represents abstract graphics element determined by vertices.
 *
 * Does not imply how the vertices are connected nor color and fill.
 */
export interface Mesh {
    /**
     * Array of original vertices
     */
    vertices: Vertex[];
    /**
     * Mesh elements
     */
    elements: MeshElement[];
    /**
     * Mesh data like colors
     */
    props?: unknown;
}

