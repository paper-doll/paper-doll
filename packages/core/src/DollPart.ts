/**
 * @paper-doll/core
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { BoundMesh } from "./BoundMesh";

/**
 * Part of the paperdoll.
 */
export interface DollPart {
    /**
     * Id of the part
     */
    id: string;

    /**
     * Background graphics.
     * This will be drawn in the index order
     */
    background: BoundMesh[];

    /**
     * Foreground graphics.
     * This will be drawn in the index order
     */
    foreground: BoundMesh[];
}
