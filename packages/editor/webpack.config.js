const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require("html-webpack-plugin");

const outputPath = path.resolve(__dirname, 'static');

module.exports = {
  mode: 'development',
  entry: [
    './src/index.tsx',
  ],
  resolve: {
    extensions: ['.tsx', '.ts', '.jsx', '.js', '.json']
  },
  output: {
    filename: 'paperdoll-editor.js',
    path: outputPath,
  },
  devtool: 'inline-source-map',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: [
          'ts-loader'
        ],
        exclude: [/node_modules/, /\.spec\.tsx?$/],
      },
      {
        test: /\.css$/i,
        use: ["style-loader", "css-loader"],
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        type: "asset/resource", // TODO: Inline?
      }
    ],
  },
  plugins: [
    new HtmlWebpackPlugin(),
  ],
  devServer: {
    static: outputPath,
    host: 'localhost',
    port: 3000,
    historyApiFallback: true,
    hot: true,
  },
};