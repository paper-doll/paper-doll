/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import produce from "immer";
import { Action } from "redux";
import { EditorState, pathToSelection, Selection } from "../model";
import { registerAction } from "../rootReducer";

const ACTION_TYPE = "select";

/**
 * Data for update Vertex action
 */
export interface SelectAction extends Action {
    /** Type of action */
    type: typeof ACTION_TYPE,
    /** Path indicating what to select */
    selection: Selection;
}

/**
 * Creates new select action
 * @param path - Path indicating what to select
 * @returns Select Action
 */
export function select(path: string): SelectAction;
export function select(selection: Selection): SelectAction;
export function select(data: string | Selection): SelectAction {

    const selection = (typeof data === "string")
        ? pathToSelection(data)
        : data as Selection;
    return {
        type: ACTION_TYPE,
        selection
    };
}

/**
 * Selects object
 * @param state
 * @param action
 */
function actionHandler(state: EditorState, action: SelectAction): EditorState {
    return produce(state, s => {
        s.selection = action.selection;
    });
}

registerAction(ACTION_TYPE, actionHandler);