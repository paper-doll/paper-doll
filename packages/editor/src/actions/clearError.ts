/**
 * @paper-doll/editor
 * Copyright © 2021 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Action } from "redux";
import { EditorState } from "../model/EditorState";
import { registerAction } from "../rootReducer";

const ACTION_TYPE = "clear_error";

/**
 * Creates action to clear error
 */
export const clearError = (): Action<string> => ({
    type: ACTION_TYPE
});

/**
 * Clears error
 * @param state
 * @param action
 */
function actionHandler(state: EditorState, action: Action): EditorState {
    return {
        ...state,
        error: undefined
    };
}

registerAction(ACTION_TYPE, actionHandler);
