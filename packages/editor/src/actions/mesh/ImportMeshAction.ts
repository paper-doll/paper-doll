/**
 * @paper-doll/editor
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { svgToMesh } from "@paper-doll/svg-import";
import * as SVG from "@svgdotjs/svg.js";
import { AnyAction } from "redux";
import { EditorState } from "../../model/EditorState";
import { registerAction } from "../../rootReducer";

/**
 * Mesh actions
 */
const IMPORT_MESH = "import_mesh";

/**
 * Action to import mesh
 */
export interface ImportMeshAction extends AnyAction {
    /** Content to import */
    fileContent: string;
}

/**
 * Creates action to import files as mesh
 * @param files
 */
export const importMesh = (fileContent: string): ImportMeshAction => (
    {
        type: IMPORT_MESH,
        fileContent
    }
);

function importMeshHandler(state: EditorState, action: ImportMeshAction): EditorState {

    // const svgDoc = new SVG.Svg();
    // svgDoc.svg(action.fileContent);
    const svgDoc = SVG.SVG(action.fileContent).parent() as SVG.Element;
    const mesh = svgToMesh(svgDoc);
    const id = "test"; // TODO: Generate random ID (uuid, nanoid)

    return state;
    // return {
    //     ...state,
    //     mesh: {
    //         meshes: {
    //             ...state.mesh.meshes,
    //             [id]: mesh
    //         },
    //         selectedMesh: id
    //     }
    // };
}

registerAction(IMPORT_MESH, importMeshHandler);
