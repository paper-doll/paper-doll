/**
 * @paper-doll/editor
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import produce from "immer";
import { Action } from "redux";
import { EditorState } from "../../model/EditorState";
import { registerAction } from "../../rootReducer";

const ACTION_TYPE = "setDollName";

/**
 * Action to set Doll name
 */
export interface SetDollName extends Action<typeof ACTION_TYPE> {
    /** Name to set */
    name: string;
}

/**
 * Creates action to update Doll name
 * @param name - Name to set
 */
export const setDollName = (name: string): SetDollName => ({
    type: ACTION_TYPE,
    name
});

/**
 * Updates cage
 * @param state
 * @param action
 */
function actionHandler(state: EditorState, action: SetDollName): EditorState {
    return produce(state, s => {
        s.doll.name = action.name;
    });
}

registerAction(ACTION_TYPE, actionHandler);
