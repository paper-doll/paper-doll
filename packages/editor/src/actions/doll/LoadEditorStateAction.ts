/**
 * @paper-doll/editor
 * Copyright © 2021 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { produce } from "immer";
import { Action } from "redux";
import { EditorState, initialState } from "../../model/EditorState";
import { registerAction } from "../../rootReducer";

const ACTION_TYPE = "load_state";

/**
 * Data for loading doll
 */
export interface LoadEditorStateAction extends Action {
    /**
     * Data to load
     */
    data: EditorState;
}

/**
 * Creates action to load doll
 * @param vertexIndex Index of the selected face
 */
export const loadEditorState = (data: any): LoadEditorStateAction => ({
    type: ACTION_TYPE,
    data
});

/**
 * Loads doll
 * @param state
 * @param action
 */
function actionHandler(state: EditorState, action: LoadEditorStateAction): EditorState {

    return produce(state, newState => {
        return {
            ...initialState,
            ...action.data
        };
    });
}


registerAction(ACTION_TYPE, actionHandler);