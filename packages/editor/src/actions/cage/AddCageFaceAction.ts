/**
 * @paper-doll/editor
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { cloneVertices } from "@paper-doll/core";
import produce from "immer";
import { Action } from "redux";
import { EditorState, newFace } from "../../model";
import { registerAction } from "../../rootReducer";

const ACTION_TYPE = "addFace";

/**
 * Action to add new face to given cage
 */
export interface AddFace extends Action<typeof ACTION_TYPE> {
    /** Id of cage to add the face to */
    cageId: string;
}

/**
 * Creates action to add new face to currently selected cage
 */
export const addCageFace = (cageId: string): AddFace => ({
    type: ACTION_TYPE,
    cageId
});

/**
 * Adds new face to cage
 * @param state
 * @param action
 */
function addCageFaceHandler(state: EditorState, action: AddFace): EditorState {

    if (!action.cageId) {
        throw new Error(`Cage Id for action ${action.type} not defined!`);
    }

    const cageId = action.cageId;

    return produce(state, s => {
        const face = newFace(s.doll);
        // Add face to cage
        s.doll.cages.items[cageId].faces[face.id] = face;
        s.selection = {
            type: "C",
            cageId: cageId,
            faceId: face.id,
        };
        // Add face to morphs
        for (const morphId in s.doll.morphs.items) {
            const morph = s.doll.morphs.items[morphId];
            if (!morph.cages.includes(cageId)) continue;
            for(const keyId in morph.keys) {
                const key = morph.keys[keyId];
                key.cages[cageId].faces[face.id] = cloneVertices(face.vertices);
            }
        }
    });
}

registerAction(ACTION_TYPE, addCageFaceHandler);
