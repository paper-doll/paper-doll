/**
 * @paper-doll/editor
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import produce from "immer";
import { Action } from "redux";
import { EditorState } from "../../model/EditorState";
import { registerAction } from "../../rootReducer";

const ACTION_TYPE = "setCageName";

/**
 * Action to set Name for cage
 */
export interface SetCageName extends Action<typeof ACTION_TYPE> {
    /** Id of the cage */
    cageId: string;
    /** Name to set */
    name: string;
}

/**
 * Creates action to update selected Cage
 * @param cageId - Id of the cage to update
 * @param name - Name to set
 */
export const setCageName = (cageId: string, name: string): SetCageName => ({
    type: ACTION_TYPE,
    cageId,
    name
});

/**
 * Updates cage
 * @param state
 * @param action
 */
function actionHandler(state: EditorState, action: SetCageName): EditorState {

    if (!action.cageId) {
        throw new Error(`Cage Id for action ${action.type} not defined!`);
    }

    return produce(state, s => {
        const cageId = action.cageId!;
        s.doll.cages.items[cageId].name = action.name;
    });
}

registerAction(ACTION_TYPE, actionHandler);
