/**
 * @paper-doll/editor
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import produce from "immer";
import { Action } from "redux";
import { EditorState } from "../../model/EditorState";
import { registerAction } from "../../rootReducer";

const ACTION_TYPE = "deleteFace";

/**
 * Action to delete cage face
 */
export interface DeleteFace extends Action<typeof ACTION_TYPE> {
    /** Id of the cage to delete face from */
    cageId: string;
    /** Id of the face to delete */
    faceId: string;
}

/**
 * Creates action to remove face from currently selected cage
 *
 * @param faceId - Id of the face to remove
 */
export const deleteCageFace = (cageId: string, faceId: string): DeleteFace => ({
    type: ACTION_TYPE,
    cageId,
    faceId,
});

/**
 * Removes face from cage
 * @param state
 * @param action
 */
function deleteFaceHandler(state: EditorState, action: DeleteFace): EditorState {

    if (!action.cageId) {
        throw new Error(`Cage Id for action ${action.type} not defined!`);
    }

    if (!action.faceId) {
        throw new Error(`Face Id for action ${action.type} not defined!`);
    }

    return produce(state, s => {
        const cageId = action.cageId!;
        const faceId = action.faceId!;

        const cage = s.doll.cages.items[cageId];

        // At least one face is required
        const faceIds = Object.keys(cage.faces);
        if (faceIds.length <= 1) {
            throw new Error(`Cannot delete face ${faceId}. At least one face is required!`);
        }

        // Remove face
        delete cage.faces[faceId];

        // Set previous face as current
        if (s.selection.type === "C" && s.selection.faceId === faceId) {
            const index = faceIds.indexOf(faceId);
            const newIndex = index === 0 ? 0 : index - 1;
            s.selection.faceId = faceIds[newIndex];
        }

        // Remove face from morphs
        for (const morphId in s.doll.morphs) {
            const morph = s.doll.morphs.items[morphId];
            if (!morph.cages.includes(cageId)) continue;
            for (const keyId in morph.keys) {
                const key = morph.keys[keyId];
                delete key.cages[cageId].faces[faceId];
            }
        }
    });
}

registerAction(ACTION_TYPE, deleteFaceHandler);
