/**
 * @paper-doll/editor
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import produce from "immer";
import { Action } from "redux";
import { EditorState } from "../../model/EditorState";
import { registerAction } from "../../rootReducer";

const ACTION_TYPE = "deleteCage";

/**
 * Action to delete cage
 */
export interface DeleteCage extends Action<typeof ACTION_TYPE> {
    /** Id of cage to delete */
    cageId: string;
}

/**
 * Creates action to remove cage
 * @param cageId - Id of the cage to remove
 */
export const deleteCage = (cageId: string): DeleteCage => ({
    type: ACTION_TYPE,
    cageId,
});

/**
 * Removes cage
 * @param state
 * @param action
 */
function deleteCageHandler(state: EditorState, action: DeleteCage): EditorState {

    if (!action.cageId) {
        throw new Error(`Cage Id for action ${action.type} not defined!`);
    }

    const length = Object.keys(state.doll.cages.items).length;
    if (length <= 1) {
        throw new Error(`Cannot delete cage ${action.cageId}. At least one cage is required!`);
    }

    return produce(state, s => {
        const cageId = action.cageId!;
        delete s.doll.cages.items[cageId];
        // Switch selection if cage was selected
        if (s.selection.type === "C" && s.selection.cageId === cageId) {
            s.selection.cageId = Object.keys(s.doll.cages.items)[0];
        }
        // Delete cage from morphs
        for (const morphId in s.doll.morphs) {
            const morph = s.doll.morphs.items[morphId];
            if (!morph.cages.includes(cageId)) continue;
            for(const keyId in morph.keys) {
                const key = morph.keys[keyId];
                delete key.cages[cageId];
            }
        }
    });
}

registerAction(ACTION_TYPE, deleteCageHandler);
