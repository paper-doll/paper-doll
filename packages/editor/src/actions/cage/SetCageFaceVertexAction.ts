/**
 * @paper-doll/editor
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Vertex } from "@paper-doll/core";
import produce from "immer";
import { Action } from "redux";
import { assertIndexIsInBounds } from "../../model";
import { EditorState } from "../../model/EditorState";
import { registerAction } from "../../rootReducer";

const ACTION_TYPE = "setVertex";

/**
 * Action to set vertex of cage face.
 */
 export interface SetFaceVertex extends Action<typeof ACTION_TYPE> {
    cageId: string;
    faceId: string;
    vertexIndex: number;
    value: Vertex;
}

/**
 * Creates action to update cage face vertex (by setting index to cage vertices)
 *
 * @param vertex Index of the vertex to use
 */
export const setCageFaceVertex = (
    cageId: string,
    faceId: string,
    vertexIndex: number,
    value: Vertex
): SetFaceVertex => ({
    type: ACTION_TYPE,
    cageId,
    faceId,
    vertexIndex,
    value
});

/**
 * Sets vertex in cage face
 * @param state
 * @param action
 */
function updateVertexHandler(state: EditorState, action: SetFaceVertex): EditorState {

    if (!action.cageId) {
        throw new Error(`Cage Id for action ${action.type} not defined!`);
    }

    if (!action.faceId) {
        throw new Error(`Face Id for action ${action.type} not defined!`);
    }

    if (action.vertexIndex === undefined) {
        throw new Error(`Vertex Index for action ${action.type} not defined!`);
    }

    if (action.value === undefined) {
        throw new Error(`Vertex value for action ${action.type} not defined!`);
    }

    return produce(state, s => {
        const cageId = action.cageId!;
        const faceId = action.faceId!;
        const vertexId = action.vertexIndex!;

        const face = s.doll.cages.items[cageId].faces[faceId];

        assertIndexIsInBounds(face, vertexId);

        face.vertices[vertexId] = action.value;
    });
}

registerAction(ACTION_TYPE, updateVertexHandler);
