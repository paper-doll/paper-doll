/**
 * @paper-doll/editor
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import produce from "immer";
import { Action } from "redux";
import { newCage } from "../../model/ECage";
import { EditorState } from "../../model/EditorState";
import { registerAction } from "../../rootReducer";

const ACTION_TYPE = "addCage";

/**
 * Action to create new cage
 */
export interface AddCage extends Action<typeof ACTION_TYPE> { };

/**
 * Creates action to add new cage
 */
export const addCage = (): AddCage => ({
    type: ACTION_TYPE,
});

/**
 * Creates new cage
 * @param state
 * @param action
 */
function addCageHandler(state: EditorState, action: AddCage): EditorState {
    return produce(state, s => {
        const cage = newCage(s.doll);
        s.doll.cages.items[cage.id] = cage;
        s.selection = {
            type: "C",
            cageId: cage.id
        }
    });
}

registerAction(ACTION_TYPE, addCageHandler);
