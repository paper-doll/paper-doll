/**
 * @paper-doll/editor
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import produce from "immer";
import { Action } from "redux";
import { EditorState } from "../../model/EditorState";
import { registerAction } from "../../rootReducer";

const ACTION_TYPE = "setFaceName";

/**
 * Action to set face name
 */
export interface SetFaceNameAction extends Action<typeof ACTION_TYPE> {
    cageId: string;
    faceId: string;
    name: string;
}

/**
 * Creates action to update selected Face
 * @param value New name of the cage
 */
export const setFaceName = (cageId: string, faceId: string, name: string) : SetFaceNameAction => ({
    type: ACTION_TYPE,
    cageId,
    faceId,
    name
});

/**
 * Updates cage face
 * @param state
 * @param action
 */
function actionHandler(state: EditorState, action: SetFaceNameAction): EditorState {

    if (!action.cageId) {
        throw new Error(`Cage Id for action ${action.type} not defined!`);
    }

    if (!action.faceId) {
        throw new Error(`Face Id for action ${action.type} not defined!`);
    }

    return produce(state, s => {
        const cageId = action.cageId!;
        const faceId = action.faceId!;
        s.doll.cages.items[cageId].faces[faceId].name = action.name;
    });
}

registerAction(ACTION_TYPE, actionHandler);
