/**
 * @paper-doll/editor
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import produce from "immer";
import { Action } from "redux";
import { assertIndexIsInBounds } from "../../model";
import { EditorState } from "../../model/EditorState";
import { registerAction } from "../../rootReducer";

const ACTION_TYPE = "deleteVertex";

/**
 * Action to delete face vertex
 */
export interface DeleteFaceVertex extends Action<typeof ACTION_TYPE> {
    cageId: string;
    faceId: string;
    vertexIndex: number;
}

/**
 * Creates action to remove vertex from currently selected cage and face
 */
export const deleteCageFaceVertex = (
    cageId: string,
    faceId: string,
    vertexIndex: number
): DeleteFaceVertex => ({
    type: ACTION_TYPE,
    cageId,
    faceId,
    vertexIndex
});

/**
 * Removes vertex from cage
 * @param state
 * @param action
 */
function deleteVertexHandler(state: EditorState, action: DeleteFaceVertex): EditorState {

    if (!action.cageId) {
        throw new Error(`Cage Id for action ${action.type} not defined!`);
    }

    if (!action.faceId) {
        throw new Error(`Face Id for action ${action.type} not defined!`);
    }

    if (action.vertexIndex === undefined) {
        throw new Error(`Vertex Index for action ${action.type} not defined!`);
    }

    return produce(state, s => {
        const cageId = action.cageId!;
        const faceId = action.faceId!;
        const vertexId = action.vertexIndex!;

        const face = s.doll.cages.items[cageId].faces[faceId];

        assertIndexIsInBounds(face, vertexId);

        const length = face.vertices.length;
        if (length <= 3) {
            throw new Error(`Cannot delete vertex ${vertexId}. Face ${faceId} must have at least 3 vertices!`);
        }

        // Remove vertex from face
        face.vertices.splice(vertexId, 1);

        // Remove vertex from morphs
        for (const morphId in s.doll.morphs) {
            const morph = s.doll.morphs.items[morphId];
            if (!morph.cages.includes(cageId)) continue;
            for (const keyId in morph.keys) {
                const key = morph.keys[keyId];
                key.cages[cageId].faces[faceId]
                    .splice(vertexId, 1);
            }
        }
    });
}

registerAction(ACTION_TYPE, deleteVertexHandler);
