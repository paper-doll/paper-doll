/**
 * @paper-doll/editor
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Vertex } from "@paper-doll/core";
import { produce } from "immer";
import { Action } from "redux";
import { assertIndexIsInBoundsForAdd, getNewVertexValue } from "../../model";
import { EditorState } from "../../model/EditorState";
import { registerAction } from "../../rootReducer";

const ACTION_TYPE = "addVertex";

/**
 * Action to add new vertex to cage face.
 * If vertex value is not specified, tries to split
 * the face segment corresponding starting on the face index.
 */
export interface AddFaceVertex extends Action<typeof ACTION_TYPE> {
    cageId: string;
    faceId: string;
    vertexIndex: number;
    value?: Vertex;
}

/**
 * Creates action to add new face vertex to currently selected face
 *
 * @param atIndex - 0 based index where to add the vertex
 * @param vertex - New vertex value
 */
export const addCageFaceVertex = (
    cageId: string,
    faceId: string,
    vertexIndex: number,
    value?: Vertex
): AddFaceVertex => ({
    type: ACTION_TYPE,
    cageId,
    faceId,
    vertexIndex,
    value
});

/**
 * Adds new cage vertex to face
 * @param state
 * @param action
 */
function addFaceVertexHandler(state: EditorState, action: AddFaceVertex): EditorState {

    if (!action.cageId) {
        throw new Error(`Cage Id for action ${action.type} not defined!`);
    }

    if (!action.faceId) {
        throw new Error(`Face Id for action ${action.type} not defined!`);
    }

    return produce(state, s => {
        const cageId = action.cageId!;
        const faceId = action.faceId!;

        const face = s.doll
            .cages.items[cageId]
            .faces[faceId];

        const vertexId = action.vertexIndex ?? face.vertices.length;
        assertIndexIsInBoundsForAdd(face, vertexId);

        const vertex = action.value ?? getNewVertexValue(face, vertexId);

        // Add vertex to face
        face.vertices.splice(vertexId, 0, vertex);

        // Add vertex to morphs
        for (const morphId in s.doll.morphs) {
            const morph = s.doll.morphs.items[morphId];
            if (!morph.cages.includes(cageId)) continue;
            for (const keyId in morph.keys) {
                const key = morph.keys[keyId];
                key.cages[cageId].faces[faceId]
                    .splice(vertexId, 0, [...vertex]);
            }
        }
    });
}

registerAction(ACTION_TYPE, addFaceVertexHandler);
