/**
 * @paper-doll/editor
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import produce from "immer";
import { Action } from "redux";
import { EditorState } from "../../model/EditorState";
import { registerAction } from "../../rootReducer";

const ACTION_TYPE = "setCageColor";

/**
 * Action to set color for cage
 */
export interface SetCageColor extends Action<typeof ACTION_TYPE> {
    /** Id of the cage */
    cageId: string;
    /** Color to set */
    color: string;
}

/**
 * Creates action to update selected Cage
 * @param cageId - Id of the cage to update
 * @param color - Color to set
 */
export const setCageColor = (cageId: string, color: string): SetCageColor => ({
    type: ACTION_TYPE,
    cageId,
    color
});

/**
 * Updates cage
 * @param state
 * @param action
 */
function actionHandler(state: EditorState, action: SetCageColor): EditorState {

    if (!action.cageId) {
        throw new Error(`Cage Id for action ${action.type} not defined!`);
    }

    return produce(state, s => {
        const cageId = action.cageId!;
        s.doll.cages.items[cageId].color = action.color;
    });
}

registerAction(ACTION_TYPE, actionHandler);
