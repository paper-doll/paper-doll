/**
 * @paper-doll/editor
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import produce from "immer";
import { Action } from "redux";
import { EPathMesh, PathMeshType } from "../../model";
import { EditorState } from "../../model/EditorState";
import { registerAction } from "../../rootReducer";
import { MeshSelection } from "./Selections";

const ACTION_TYPE = "setPathMeshColor";

/**
 * Action to set Name for mesh
 */
export interface SetPathMeshColor extends MeshSelection, Action<typeof ACTION_TYPE> {
    /** Color to set */
    color: string;
}

/**
 * Creates action to update selected mesh
 * @param mesh - Mesh selection
 * @param color - Color to set
 */
export const setPathMeshColor = (
    mesh: MeshSelection,
    color: string
): SetPathMeshColor => ({
    ...mesh,
    type: ACTION_TYPE,
    color
});

/**
 * Updates mesh color
 * @param state
 * @param action
 */
function actionHandler(state: EditorState, action: SetPathMeshColor): EditorState {

    if (!action.partId) {
        throw new Error(`Part Id for action ${action.type} not defined!`);
    }

    if (!action.placement) {
        throw new Error(`Placement for action ${action.type} not defined!`);
    }

    if (!action.meshId) {
        throw new Error(`Mesh Id for action ${action.type} not defined!`);
    }

    return produce(state, s => {
        const part = s.doll.parts.items[action.partId]
        const container = action.placement === "F"
            ? part.foreground
            : part.background;
        const mesh = container.items[action.meshId];
        if (mesh.type !== PathMeshType) {
            throw new Error(`Mesh is not Path Mesh!`);
        }
        (mesh as EPathMesh).props.color = action.color;
    });
}

registerAction(ACTION_TYPE, actionHandler);
