/**
 * @paper-doll/editor
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import produce from "immer";
import { Action } from "redux";
import { EditorState } from "../../model/EditorState";
import { registerAction } from "../../rootReducer";
import { MeshSelection } from "./Selections";

const ACTION_TYPE = "setMeshName";

/**
 * Action to set Name for mesh
 */
export interface SetMeshName extends MeshSelection, Action<typeof ACTION_TYPE> {
    /** Name to set */
    name: string;
}

/**
 * Creates action to update selected part
 * @param partId - Id of the part to update
 * @param name - Name to set
 */
export const setMeshName = (
    mesh: MeshSelection,
    name: string
): SetMeshName => ({
    ...mesh,
    type: ACTION_TYPE,
    name
});

/**
 * Updates mesh name
 * @param state
 * @param action
 */
function actionHandler(state: EditorState, action: SetMeshName): EditorState {

    if (!action.partId) {
        throw new Error(`Part Id for action ${action.type} not defined!`);
    }

    if (!action.placement) {
        throw new Error(`Placement for action ${action.type} not defined!`);
    }

    if (!action.meshId) {
        throw new Error(`Mesh Id for action ${action.type} not defined!`);
    }

    return produce(state, s => {
        const part = s.doll.parts.items[action.partId]
        const container = action.placement === "F"
            ? part.foreground
            : part.background;
        const mesh = container.items[action.meshId];
        mesh.name = action.name;
    });
}

registerAction(ACTION_TYPE, actionHandler);
