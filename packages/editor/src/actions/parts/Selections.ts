import { PlacementType } from "../../model";

/**
 * Mesh identification
 */
export interface MeshSelection {
    /** Id of the part to delete the mesh from */
    partId: string;
    /** Should delete mesh to foreground or background? */
    placement: PlacementType;
    /** Id of mesh to delete */
    meshId: string;
}

/**
 * Mesh segment identification
 */
export interface MeshSegmentSelection extends MeshSelection {
    /** Index of the mesh segment */
    elementId: number;
}

/**
 * Mesh segment vertex identification
 */
 export interface MeshSegmentVertexSelection extends MeshSegmentSelection {
    /** Index of the mesh segment */
    vertexId: number;
}