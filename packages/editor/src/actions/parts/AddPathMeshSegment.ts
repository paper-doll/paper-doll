/**
 * @paper-doll/editor
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { PathSegmentType, Vertex } from "@paper-doll/core";
import produce from "immer";
import { Action } from "redux";
import { EPathMeshElement, findFaceId, newClosePath, newSmoothCubicTo, newCubicTo, newLineTo, newMoveTo, newSmoothQuadraticTo, newQuadraticTo } from "../../model";
import { EditorState } from "../../model/EditorState";
import { registerAction } from "../../rootReducer";
import { MeshSegmentSelection } from "./Selections";

const ACTION_TYPE = "addPathMeshSegment";

/**
 * Action to create new path mesh segment
 */
export interface AddPathMeshSegment extends MeshSegmentSelection, Action<typeof ACTION_TYPE> {
    /** Type of segment to add */
    segmentType: PathSegmentType;
    /** Vertex value */
    vertex?: Vertex;
};

/**
 * Creates action to add new mesh segment
 */
export const addPathMeshSegment = (
    segment: MeshSegmentSelection,
    segmentType: PathSegmentType,
    vertex?: Vertex
): AddPathMeshSegment => ({
    ...segment,
    type: ACTION_TYPE,
    segmentType,
    vertex
});

/**
  * Creates new mesh segment
  * @param state
  * @param action
  */
function addPathMeshSegmentHandler(state: EditorState, action: AddPathMeshSegment): EditorState {

    return produce(state, s => {
        const part = s.doll.parts.items[action.partId];
        const container = action.placement === "B"
            ? part.background
            : part.foreground;
        const mesh = container.items[action.meshId];
        const segments = mesh.elements;

        const vertex = action.vertex
            ?? [Math.trunc(state.doll.width / 2), Math.trunc(state.doll.height / 2)];

        const faceId = findFaceId(s.doll.cages.items[mesh.cageId], vertex);

        function getSegment(segmentType: PathSegmentType): EPathMeshElement {
            switch (segmentType) {
                case "C":
                    return newCubicTo(faceId, vertex, [...vertex], [...vertex]);
                case "L":
                    return newLineTo(faceId, vertex)
                case "M":
                    return newMoveTo(faceId, vertex)
                case "Q":
                    return newQuadraticTo(faceId, vertex, [...vertex]);
                case "S":
                    return newSmoothCubicTo(faceId, vertex, [...vertex]);
                case "T":
                    return newSmoothQuadraticTo(faceId, vertex);
                case "Z":
                    return newClosePath();
            }
        }

        const segmentId = action.elementId ?? segments.length;
        const segment = getSegment(action.segmentType);
        segments.splice(segmentId, 0, segment);

        s.selection = {
            type: "P",
            partId: part.id,
            placement: action.placement,
            meshId: action.meshId,
            elementId: segmentId,
        };
    });
}

registerAction(ACTION_TYPE, addPathMeshSegmentHandler);
