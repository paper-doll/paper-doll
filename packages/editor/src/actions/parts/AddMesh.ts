/**
 * @paper-doll/editor
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import produce from "immer";
import { Action } from "redux";
import { newMesh, PlacementType } from "../../model";
import { EditorState } from "../../model/EditorState";
import { registerAction } from "../../rootReducer";

const ACTION_TYPE = "addMesh";

/**
 * Action to create new Mesh
 */
export interface AddMesh extends Action<typeof ACTION_TYPE> {
    /** Id of the part to add the mesh to */
    partId: string;
    /** Should add mesh to foreground or background? */
    target: PlacementType;
    /** Type of mesh to add */
    meshType: string;
    /** Id of governing cage */
    cageId: string;
};

/**
 * Creates action to add new mesh to foreground
 */
export const addForegroundMesh = (partId: string, meshType: string, cageId: string): AddMesh => ({
    type: ACTION_TYPE,
    partId,
    target: "F",
    meshType,
    cageId
});

/**
  * Creates action to add new mesh to background
  */
export const addBackgroundMesh = (partId: string, meshType: string, cageId: string): AddMesh => ({
    type: ACTION_TYPE,
    partId,
    target: "B",
    meshType,
    cageId
});

/**
  * Creates new mesh
  * @param state
  * @param action
  */
function addPartHandler(state: EditorState, action: AddMesh): EditorState {
    return produce(state, s => {
        const part = s.doll.parts.items[action.partId];
        const container = action.target === "B"
            ? part.background
            : part.foreground;

        const mesh = newMesh(action.meshType, action.cageId);
        container.items[mesh.id] = mesh;
        s.selection = {
            type: "P",
            partId: part.id,
            placement: action.target,
            meshId: mesh.id
        };
    });
}

registerAction(ACTION_TYPE, addPartHandler);
