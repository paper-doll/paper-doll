/**
 * @paper-doll/editor
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import produce from "immer";
import { Action } from "redux";
import { EditorState } from "../../model/EditorState";
import { registerAction } from "../../rootReducer";

const ACTION_TYPE = "setPartName";

/**
 * Action to set Name for part
 */
export interface SetPartName extends Action<typeof ACTION_TYPE> {
    /** Id of the part */
    partId: string;
    /** Name to set */
    name: string;
}

/**
 * Creates action to update selected part
 * @param partId - Id of the part to update
 * @param name - Name to set
 */
export const setPartName = (partId: string, name: string): SetPartName => ({
    type: ACTION_TYPE,
    partId,
    name
});

/**
 * Updates part
 * @param state
 * @param action
 */
function actionHandler(state: EditorState, action: SetPartName): EditorState {

    if (!action.partId) {
        throw new Error(`Part Id for action ${action.type} not defined!`);
    }

    return produce(state, s => {
        const partId = action.partId;
        s.doll.parts.items[partId].name = action.name;
    });
}

registerAction(ACTION_TYPE, actionHandler);
