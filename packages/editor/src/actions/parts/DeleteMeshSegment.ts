/**
 * @paper-doll/editor
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import produce from "immer";
import { Action } from "redux";
import { PlacementType } from "../../model";
import { EditorState } from "../../model/EditorState";
import { registerAction } from "../../rootReducer";
import { MeshSegmentSelection } from "./Selections";

const ACTION_TYPE = "deleteMeshSegment";

/**
 * Action to create new path mesh segment
 */
export interface DeleteMeshSegment extends MeshSegmentSelection, Action<typeof ACTION_TYPE>  {
};

/**
 * Creates action to delete mesh segment
 */
export const deleteMeshSegment = (
    segment: MeshSegmentSelection,
): DeleteMeshSegment => ({
    ...segment,
    type: ACTION_TYPE,
});

/**
  * Deletes mesh segment
  * @param state
  * @param action
  */
function deleteMeshSegmentHandler(state: EditorState, action: DeleteMeshSegment): EditorState {

    return produce(state, s => {
        const part = s.doll.parts.items[action.partId];
        const container = action.placement === "B"
            ? part.background
            : part.foreground;
        const mesh = container.items[action.meshId];
        const segments = mesh.elements;

        segments.splice(action.elementId, 1);
    });
}

registerAction(ACTION_TYPE, deleteMeshSegmentHandler);
