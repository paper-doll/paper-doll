/**
 * @paper-doll/editor
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { PathSegmentType } from "@paper-doll/core";
import produce from "immer";
import { Action } from "redux";
import { EditorState } from "../../model/EditorState";
import { registerAction } from "../../rootReducer";
import { MeshSegmentSelection } from "./Selections";

const ACTION_TYPE = "setPathMeshSegmentType";

/**
 * Action to update existing path mesh segment
 */
export interface SetPathMeshSegmentType extends MeshSegmentSelection, Action<typeof ACTION_TYPE> {
    /** Type of segment to add */
    segmentType: PathSegmentType;
};

/**
 * Creates action to set type of path mesh segment
 */
export const setPathMeshSegmentType = (
    segment: MeshSegmentSelection,
    segmentType: PathSegmentType
): SetPathMeshSegmentType => ({
    ...segment,
    type: ACTION_TYPE,
    segmentType,
});

/**
  * Creates new mesh
  * @param state
  * @param action
  */
function setPathMeshSegmentTypeHandler(state: EditorState, action: SetPathMeshSegmentType): EditorState {

    return produce(state, s => {
        const part = s.doll.parts.items[action.partId];
        const container = action.placement === "B"
            ? part.background
            : part.foreground;
        const mesh = container.items[action.meshId];
        const segments = mesh.elements;

        const elementId = action.elementId ?? segments.length;
        const segment = segments[elementId];
        segment.type = action.segmentType;
    });
}

registerAction(ACTION_TYPE, setPathMeshSegmentTypeHandler);
