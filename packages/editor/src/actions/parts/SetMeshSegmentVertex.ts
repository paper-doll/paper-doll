/**
 * @paper-doll/editor
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Vertex } from "@paper-doll/core";
import produce from "immer";
import { Action } from "redux";
import { EditorState } from "../../model/EditorState";
import { registerAction } from "../../rootReducer";
import { MeshSegmentVertexSelection } from "./Selections";

const ACTION_TYPE = "setMeshSegmentVertex";

/**
 * Action to update existing path mesh segment vertex
 */
export interface SetMeshSegmentVertex extends MeshSegmentVertexSelection, Action<typeof ACTION_TYPE> {
    /** Value of the vertex */
    vertex: Vertex;
};

/**
 * Creates action to set path mesh segment vertex
 */
export const setMeshSegmentVertex = (
    segment: MeshSegmentVertexSelection,
    vertex: Vertex
): SetMeshSegmentVertex => ({
    ...segment,
    type: ACTION_TYPE,
    vertex
});

/**
  * Creates new mesh
  * @param state
  * @param action
  */
function handler(state: EditorState, action: SetMeshSegmentVertex): EditorState {

    return produce(state, s => {
        const part = s.doll.parts.items[action.partId];
        const container = action.placement === "B"
            ? part.background
            : part.foreground;
        const mesh = container.items[action.meshId];
        const segments = mesh.elements;

        const elementId = action.elementId ?? segments.length;
        const segment = segments[elementId];
        const vertex = segment.vertices[action.vertexId];
        vertex.vertex = action.vertex;
    });
}

registerAction(ACTION_TYPE, handler);
