/**
 * @paper-doll/editor
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import produce from "immer";
import { Action } from "redux";
import { EditorState } from "../../model/EditorState";
import { registerAction } from "../../rootReducer";
import { MeshSegmentVertexSelection } from "./Selections";

const ACTION_TYPE = "setMeshSegmentVertexFace";

/**
 * Action to update existing path mesh segment
 */
export interface SetMeshSegmentVertexFace extends MeshSegmentVertexSelection, Action<typeof ACTION_TYPE> {
    /** Id of the face */
    faceId: string | undefined;
};

/**
 * Creates action to set path mesh segment governing face
 */
export const setMeshSegmentVertexFace = (
    segment: MeshSegmentVertexSelection,
    faceId: string | undefined
): SetMeshSegmentVertexFace => ({
    ...segment,
    type: ACTION_TYPE,
    faceId
});

/**
  * Creates new mesh
  * @param state
  * @param action
  */
function handler(state: EditorState, action: SetMeshSegmentVertexFace): EditorState {

    return produce(state, s => {
        const part = s.doll.parts.items[action.partId];
        const container = action.placement === "B"
            ? part.background
            : part.foreground;
        const mesh = container.items[action.meshId];
        const segments = mesh.elements;

        const elementId = action.elementId ?? segments.length;
        const segment = segments[elementId];
        const vertex = segment.vertices[action.vertexId];
        vertex.faceId = action.faceId;
    });
}

registerAction(ACTION_TYPE, handler);
