/**
 * @paper-doll/editor
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import produce from "immer";
import { Action } from "redux";
import { EditorState } from "../../model/EditorState";
import { registerAction } from "../../rootReducer";
import { MeshSelection } from "./Selections";

const ACTION_TYPE = "deleteMesh";

/**
 * Action to delete new Mesh
 */
export interface DeleteMesh extends MeshSelection, Action<typeof ACTION_TYPE> {
};

/**
 * Creates action to add new mesh to foreground
 */
export const deleteMesh = (mesh: MeshSelection): DeleteMesh => ({
    ...mesh,
    type: ACTION_TYPE,
});

/**
  * Deletes mesh
  * @param state
  * @param action
  */
function addPartHandler(state: EditorState, action: DeleteMesh): EditorState {
    return produce(state, s => {
        const part = s.doll.parts.items[action.partId];
        const container = action.placement === "B"
            ? part.background
            : part.foreground;
        delete container.items[action.meshId];
    });
}

registerAction(ACTION_TYPE, addPartHandler);
