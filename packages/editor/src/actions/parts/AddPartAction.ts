/**
 * @paper-doll/editor
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import produce from "immer";
import { Action } from "redux";
import { newPart } from "../../model";
import { EditorState } from "../../model/EditorState";
import { registerAction } from "../../rootReducer";

const ACTION_TYPE = "addPart";

/**
 * Action to create new Part
 */
export interface AddPart extends Action<typeof ACTION_TYPE> { };

/**
 * Creates action to add new Part
 */
export const addPart = (): AddPart => ({
    type: ACTION_TYPE,
});

/**
 * Creates new Part
 * @param state
 * @param action
 */
function addPartHandler(state: EditorState, action: AddPart): EditorState {
    return produce(state, s => {
        const part = newPart();
        s.doll.parts.items[part.id] = part;
        s.selection = {
            type: "P",
            partId: part.id
        }
    });
}

registerAction(ACTION_TYPE, addPartHandler);
