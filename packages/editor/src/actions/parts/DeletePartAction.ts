/**
 * @paper-doll/editor
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import produce from "immer";
import { Action } from "redux";
import { EditorState } from "../../model/EditorState";
import { registerAction } from "../../rootReducer";

const ACTION_TYPE = "deletePart";

/**
 * Action to delete part
 */
export interface DeletePart extends Action<typeof ACTION_TYPE> {
    /** Id of part to delete */
    partId: string;
}

/**
 * Creates action to remove part
 * @param partId - Id of the part to remove
 */
export const deletePart = (partId: string): DeletePart => ({
    type: ACTION_TYPE,
    partId,
});

/**
 * Removes part
 * @param state
 * @param action
 */
function actionHandler(state: EditorState, action: DeletePart): EditorState {

    if (!action.partId) {
        throw new Error(`Part Id for action ${action.type} not defined!`);
    }

    // const length = Object.keys(state.doll.parts.items).length;
    // if (length <= 1) {
    //     throw new Error(`Cannot delete part ${action.partId}. At least one part is required!`);
    // }

    return produce(state, s => {
        const partId = action.partId;
        delete s.doll.parts.items[partId];
        // Switch selection if part was selected
        if (s.selection.type === "P" && s.selection.partId === partId) {
            s.selection.partId = Object.keys(s.doll.parts.items)[0];
        }
    });
}

registerAction(ACTION_TYPE, actionHandler);
