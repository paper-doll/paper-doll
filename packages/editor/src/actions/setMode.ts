/**
 * @paper-doll/editor
 * Copyright © 2021 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Action } from "redux";
import { EditorState } from "../model/EditorState";
import { EditorMode } from "../model/EditorMode";
import { registerAction } from "../rootReducer";

const ACTION_TYPE = "mode_set";

/**
 * Data for update Vertex action
 */
export interface SetModeAction extends Action {
    /**
     * New edit mode
     */
    mode: EditorMode;
}

/**
 * Creates action to set edit mode
 */
export const setMode = (mode: EditorMode): SetModeAction => ({
    type: ACTION_TYPE,
    mode
});

/**
 * Sets edit mode
 * @param state
 * @param action
 */
function actionHandler(state: EditorState, action: SetModeAction): EditorState {
    return {
        ...state,
        mode: action.mode
    };
}

registerAction(ACTION_TYPE, actionHandler);
