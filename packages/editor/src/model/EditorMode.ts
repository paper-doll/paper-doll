/**
 * @paper-doll/editor
 * Copyright © 2021 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

/**
 * Editor modes
 */
 export enum EditorMode {
    /**
     * Cage editing
     */
    Cages = 0,
    /**
     * Morph targets editing
     */
    Morphs = 1,
    /**
     * Doll parts
     */
    Parts = 2,
}