/**
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

export interface DollSelection {
    type: "D"
};

export interface CageSelection {
    type: "C";
    cageId?: string;
    faceId?: string;
    vertexId?: number;
}

export interface MorphSelection {
    type: "M";
    morphId?: string;
    keyId?: number;
    cageId?: string;
    faceId?: string;
    vertexId?: number;
}

/** Mesh placement - Foreground or Background */
export type PlacementType = "F" | "B";

/** Part selection */
export interface PartSelection {
    type: "P";
    partId?: string;
    placement?: PlacementType;
    meshId?: string;
    elementId?: number;
    vertexId?: number;
}

/** Edited item selection */
export type Selection =
    DollSelection
    | CageSelection
    | MorphSelection
    | PartSelection;

/**
 * Converts Selection to string
 * @param value
 * @returns
 */
export function selectionToString(value: Selection) {

    function combine(base: string, ...values: (string | undefined)[]) {
        let result = base;
        for (let i = 0; i < values.length; ++i) {
            if (values[i] === undefined) break;
            result = result + ":" + values[i];
        };
        return result;
    }

    switch (value.type) {
        case "C":
            return combine(
                "C",
                value.cageId,
                value.faceId,
                value.vertexId?.toString()
            );

        case "D":
            return "D";

        case "M":
            return combine(
                "M",
                value.morphId,
                value.keyId?.toString(),
                value.cageId,
                value.faceId,
                value.vertexId?.toString()
            );

        case "P":
            return combine(
                "P",
                value.partId,
                value.placement,
                value.meshId,
                value.elementId?.toString(),
                value.vertexId?.toString()
            );

        default:
            throw new Error(`Unknown selection type ${(value as any).type}`);
    }
}

function toNumber(value: string | undefined) {
    return value === undefined
        ? undefined
        : parseInt(value);
}

function toPlacement(value: string | undefined): PlacementType | undefined {
    if (value === undefined) return undefined;
    if (value === "F") return "F";
    if (value === "B") return "B";
    throw new Error(`Unknown part placement ${value}`);
}

/**
 * Converts string path to selection
 * @param path - Path to convert
 * @returns resulting selection
 */
export function pathToSelection(path: string): Selection {
    const parts = path.split(":");
    switch (parts[0]) {
        case "D":
            return { type: "D" };
        case "C":
            return {
                type: "C",
                cageId: parts[1],
                faceId: parts[2],
                vertexId: toNumber(parts[3])
            };
        case "M":
            return {
                type: "M",
                morphId: parts[1],
                keyId: toNumber(parts[2]),
                cageId: parts[3],
                faceId: parts[4],
                vertexId: toNumber(parts[5])
            };
        case "P":
            return {
                type: "P",
                partId: parts[1],
                placement: toPlacement(parts[2]),
                meshId: parts[3],
                elementId: toNumber(parts[4]),
                vertexId: toNumber(parts[5]),
            };
        default:
            throw new Error(`Unknown path '${path}'`);
    }
}