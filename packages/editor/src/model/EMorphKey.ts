/**
 * @paper-doll/editor
 * Copyright © 2021 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { cloneVertices } from "@paper-doll/core";
import { Hidable } from "./BaseInterfaces";
import { ECage } from "./ECage";
import { EMorphCage, newMorphCage } from "./EMorphCage";

/**
 * Key in morph target
 */
export interface EMorphKey extends Hidable {

    /**
     * Morph target value this key is for
     */
    value: number;

    /**
     * Cages modifications for this key.
     *
     * The key is Id of the cage.
     */
    cages: Record<string, EMorphCage>;
}

/**
 * Creates new EMorphKey for given value and cage
 *
 * @param value - Value of the new morph key
 * @param cage - Source cage to get vertices from
 */
export function newMorphKey(value: number, cage: ECage): EMorphKey {
    return {
        value,
        cages: {
            [cage.id]: newMorphCage(cage)
        },
        hidden: false,
    };
}

/**
 * Creates new EMorphKey from other key
 *
 * @param value - Value of the new morph key
 * @param original - Original morph key to take vertices from
 */
export function fromMorphKey(value: number, original: EMorphKey): EMorphKey {
    const result: EMorphKey = {
        value,
        cages: {},
        hidden: false,
    };
    for (let cageId in original.cages) {
        const cage = original.cages[cageId];
        result.cages[cageId] = {
            faces:{},
        };
        for(let faceId in cage.faces){
            const vertices = cage.faces[faceId];
            result.cages[cageId].faces[faceId] = cloneVertices(vertices);
        }
    }
    return result;
}
