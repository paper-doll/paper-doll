/**
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Hidable, Identifiable } from "./BaseInterfaces";

/**
 * Represents record<string,T>
 * with some additional properties
 */
export interface Container<T> extends Identifiable, Hidable {
    /**
     * The items in the container
     */
    items: Record<string, T>;
}