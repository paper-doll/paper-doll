/**
 * @paper-doll/editor
 * Copyright © 2021 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { PathSegmentType, Vertex } from "@paper-doll/core";
import { nanoid } from "nanoid";
import { registerMeshType } from "./MeshCatalog";
import { EMesh, EMeshElement } from "./EMesh";

/**
 * Path mesh type
 */
export const PathMeshType = "p";

/**
 * Type discriminator of path mesh
 */
export type EPathMeshType = typeof PathMeshType;

/**
 * Path specific elements represent the path segments
 *
 * Note for editor purposes we keep 3 vertices even for elements that
 * need less. This is to easier switch the type of segment.
 * Vertex [0] is control "in" if used
 * Vertex [1] is control "out" if used
 * Vertex [2] is the "end of line" position
 */
export interface EPathMeshElement extends EMeshElement {
    /**
     * Type
     */
    type: PathSegmentType;
}

/**
 * Additional mesh properties for the path
 */
export interface EPathMeshProps {

    /**
     * Color of the path stroke
     */
    color: string;

    /**
     * Fill of the path
     */
    fillColor: string;
}

/**
 * Editable mesh that represents SVG Path
 */
export interface EPathMesh extends EMesh {
    /**
     * Type is always path
     */
    type: EPathMeshType;

    /**
     * Mesh elements helps mesh decide how to draw the vertices
     */
    elements: Array<EPathMeshElement>;

    /**
     * Path mesh properties
     */
    props: EPathMeshProps;
}

/**
 * Creates new mesh
 * @returns
 */
export function newPathMesh(cageId: string): EPathMesh {
    return {
        type: PathMeshType,
        id: nanoid(),
        name: "New Path",
        cageId,
        elements: [
            newMoveTo("", [0, 0]),
            newLineTo("", [0, 10]),
            newLineTo("", [10, 0]),
            newClosePath(),
        ],
        props: {
            color: "#000000",
            fillColor: "#ffffff"
        },
        hidden: false
    };
}

/**
 * EPathMesh type guard
 *
 * @param mesh Mesh to check
 * @returns whether the mesh is of EPathMesh type
 */
export function isEPathMesh(mesh: EMesh): mesh is EPathMesh {
    return mesh.type === PathMeshType;
}

/**
 * Creates path segment MoveTo
 * @param vertex Target point
 * @param faceId Cage Face to govern the target point
 */
export function newMoveTo(faceId: string, vertex: Vertex): EPathMeshElement {
    const result: EPathMeshElement = {
        type: "M",
        vertices: [
            { faceId, vertex },
            { faceId, vertex },
            { faceId, vertex }
        ]
    };
    return result;
}

/**
 * Creates path segment LineTo
 * @param target Target point
 * @param faceId Cage Face to govern the target point
 */
export function newLineTo(faceId: string, target: Vertex): EPathMeshElement {
    const result: EPathMeshElement = {
        type: "L",
        vertices: [
            { faceId, vertex: target },
            { faceId, vertex: target },
            { faceId, vertex: target }
        ]
    };
    return result;
}

/**
 * Creates bezier curve segment with control points.
 *
 * If no control points are specified,
 * the control points will use same value as the target vertex
 *
 * @param vertex Target point
 * @param faceId Cage Face to govern the target point
 * @param controlIn Entry control point
 * @param controlOut Exit control point
 */
export function newCubicTo(faceId: string, vertex: Vertex, controlIn?: Vertex, controlOut?: Vertex): EPathMeshElement {
    const result: EPathMeshElement = {
        type: "C",
        vertices: [
            { faceId, vertex: controlIn ?? vertex },
            { faceId, vertex: controlOut ?? vertex },
            { faceId, vertex },
        ]
    };
    return result;
}

/**
 * Creates stringed smooth bezier curve segment with exit control point.
 * Entry control point is reflected exit control point of previous segment.
 *
 * If no control point is specified, target vertex value will be used
 *
 * @param vertex Target point
 * @param faceId Cage Face to govern the target point
 * @param controlOut Exit control point
 */
export function newSmoothCubicTo(faceId: string, vertex: Vertex, controlOut?: Vertex): EPathMeshElement {
    const result: EPathMeshElement = {
        type: "S",
        vertices: [
            { faceId, vertex: controlOut ?? vertex },
            { faceId, vertex: controlOut ?? vertex },
            { faceId, vertex },
        ]
    };
    return result;
}

/**
 * Creates quadratic bezier curve segment with one control point
 * working as both entry and exit control point.
 *
 * If no control point is specified, target vertex value will be used
 *
 * @param vertex Target point
 * @param faceId Cage Face to govern the target point
 * @param control Exit control point
 */
export function newQuadraticTo(faceId: string, vertex: Vertex, control?: Vertex): EPathMeshElement {
    const result: EPathMeshElement = {
        type: "Q",
        vertices: [
            { faceId, vertex: control ?? vertex },
            { faceId, vertex: control ?? vertex },
            { faceId, vertex },
        ]
    };
    return result;
}

/**
 * Creates stringed quadratic bezier curve segment.
 * Control point is taken as mirrored exit control point of previous segment.
 *
 * @param vertex Target point
 * @param faceId Cage Face to govern the target point
 */
export function newSmoothQuadraticTo(faceId: string, vertex: Vertex): EPathMeshElement {
    const result: EPathMeshElement = {
        type: "T",
        vertices: [
            { faceId, vertex },
            { faceId, vertex },
            { faceId, vertex },
        ]
    };
    return result;
}

/**
 * Creates path segment Close (end of path)
 */
export function newClosePath(): EPathMeshElement {
    const result: EPathMeshElement = {
        type: "Z",
        vertices: [
            { vertex: [0,0]},
            { vertex: [0,0]},
            { vertex: [0,0]},
        ]
    };
    return result;
}

registerMeshType(PathMeshType, "Path", newPathMesh);
