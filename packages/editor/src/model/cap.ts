/**
 * @paper-doll/editor
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

/**
 * Caps the value between 0 and max inclusive
 * @param value
 * @param max
 */
export default function cap(value: number, max: number): number {
    return Math.max(
        0,
        Math.min(
            value,
            max
        )
    );
}
