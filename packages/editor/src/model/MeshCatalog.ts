import { EMesh } from "./EMesh";

/**
 * Mesh creator function
 */
export type MeshCreator = (cageId: string) => EMesh;

/**
 * Mesh factory registry
 */
const registry: Record<string, MeshCreator> = {};

/**
 * Mesh names for UI
 */
export const meshTypeNames : Record<string, string> = {};

/**
 * Registers mesh creator
 * @param type - Type of mesh
 * @param creator - Creator function
 */
export function registerMeshType<TType extends string>(type: TType, name: string, creator: MeshCreator): void {
    if (Object.keys(registry).indexOf(type) < 0) {
        registry[type] = creator;
        meshTypeNames[type] = name;
    }
    else {
        throw new Error(`Mesh type ${type} is already registered`);
    }
}

/**
 * Creates new mesh of given type
 * @param type - Type of mesh
 * @param cageId - Cage id
 * @returns - Created mesh
 */
export function newMesh<TType extends string>(type: TType, cageId: string): EMesh<TType> {

    const creator = registry[type];
    if (creator === undefined) {
        throw new Error(`Cannot create unknown mesh type '${type}'`);
    }

    return creator(cageId) as EMesh<TType>;
}