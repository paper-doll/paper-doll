/**
 * @paper-doll/editor
 * Copyright © 2021 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { nanoid } from "nanoid";
import { useSelector } from "react-redux";
import { newCage } from "./ECage";
import { EditorMode } from "./EditorMode";
import { EDoll } from "./EDoll";
import { newMorph } from "./EMorph";
import { Selection } from "./Selection";

/**
 * State of the editor
 */
export interface EditorState {

    /**
     * Last error
     */
    error?: string;

    /**
     * Mode of the editor
     */
    mode: EditorMode;

    /**
     * Doll data
     */
    doll: EDoll;

    /**
     * Selected object
     */
    selection: Selection;

    /**
     * Id of selected active morph
     */
    selectedActiveMorph: string;
}

const initialDollId = nanoid();
const initialDollSize = {
    width: 250,
    height: 500
};

const initialCage = newCage(initialDollSize, "Default Cage");
const initialMorph = newMorph(initialCage, "Default Morph");

/**
 * Initial state
 */
export const initialState: EditorState = {
    // General state
    error: undefined,
    mode: EditorMode.Cages,
    // Doll data
    doll: {
        id: initialDollId,
        name: "Paper Doll",
        width: initialDollSize.width,
        height: initialDollSize.height,
        cages: {
            id: "C",
            items: {
                [initialCage.id]: initialCage,
            },
            hidden: false,
        },
        morphs: {
            id: "M",
            items: {
                [initialMorph.id]: initialMorph
            },
            hidden: false,
        },
        parts: {
            id: "P",
            items: {},
            hidden: false,
        },
        activeMorphs: {
            [initialMorph.id]: 0,
        },
    },
    selection: {
        type: "D",
    },
    selectedActiveMorph: initialMorph.id,
}

/**
 * Typed editor state selector
 */
export type editorStateSelector<T> = (state: EditorState) => T;

/**
 * Editor state hook
 */
export function useEditorState<T>(s: editorStateSelector<T>): T {
    return useSelector(s) as T;
}
