/**
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { cloneVertices, Vertex } from "@paper-doll/core";
import { ECage } from "./ECage";

/**
 * Cage modification in the morph
 */
export interface EMorphCage {
    /**
     * Cage faces
     *
     * Key is Id of the face
     * Value is array of modified vertices
     */
    faces: Record<string, Array<Vertex>>;
}

/**
 * Creates new unmodified morph cage
 * @param cage - Original cage
 * @returns EMorphCage clone of the original cage
 */
export function newMorphCage(cage: ECage): EMorphCage {
    const result: EMorphCage = {
        faces: {},
    };
    // We need deep copy the vertices to the morph
    for (const faceId in cage.faces) {
        if (Object.prototype.hasOwnProperty.call(cage.faces, faceId)) {
            const face = cage.faces[faceId];
            result.faces[face.id] = cloneVertices(face.vertices);
        }
    }
    return result;
}