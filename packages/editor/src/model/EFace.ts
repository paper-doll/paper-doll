/**
 * @paper-doll/editor
 * Copyright © 2021 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import Flatten from "@flatten-js/core";
import { RectangleSize, Vertex } from "@paper-doll/core";
import moize from "moize";
import { nanoid } from "nanoid";
import { Hidable, Identifiable, Nameable } from "./BaseInterfaces";

/**
 * Editable Cage Face
 */
export interface EFace extends Identifiable, Nameable, Hidable {
    /**
     * Face vertices
     */
    vertices: Array<Vertex>;
}

/**
 * Creates new editable face
 *
 * @param name - Name of the face (optional)
 * @returns New face
 */
export function newFace(dollSize: RectangleSize, name?: string): EFace {
    const id = nanoid();

    // Make the new face a box in the middle of the doll space
    const left = Math.trunc(dollSize.width / 4);
    const right = 3 * left;
    const top = Math.trunc(dollSize.height / 4);
    const bottom = 3 * top;

    return {
        id: id,
        name: name ?? "Face",
        vertices: [[left, top], [right, top], [right, bottom], [left, bottom]],
        hidden: false,
    }
}

/**
 * Throws error if index is out of bounds for given face.
 * The allowed range is <0; face.vertices.length)
 * @param face
 * @param index
 */
export function assertIndexIsInBounds(face: EFace, index: number): void | never {
    const length = face.vertices.length;
    if (index >= length || index < 0) {
        throw Error(`Index ${index} out of bounds for face ${face.id}`);
    }
}

/**
 * Throws error if index is out of bounds for adding to given face.
 * The allowed range is <0; face.vertices.length>
 * @param face
 * @param index
 */
export function assertIndexIsInBoundsForAdd(face: EFace, index: number): void | never {
    const length = face.vertices.length;
    if (index > length || index < 0) {
        throw Error(`Index ${index} out of bounds for face ${face.id}`);
    }
}

/**
 * Calculates new vertex position between vertex on
 * given index and previous vertex
 *
 * @param face
 * @param index
 */
export function getNewVertexValue(face: EFace, index: number): Vertex {

    assertIndexIsInBoundsForAdd(face, index);

    const length = face.vertices.length;

    const previousIndex = (index == 0 ? length : index) - 1;
    const previousVertex = face.vertices[previousIndex];

    const nextIndex = (index == length ? 0 : index);
    const vertex = face.vertices[nextIndex];

    const newX = Math.trunc((previousVertex[0] + vertex[0]) / 2);
    const newY = Math.trunc((previousVertex[1] + vertex[1]) / 2);

    return [newX, newY];
}

function convertFaceToPoly(face: EFace): Flatten.Polygon {
    const p = new Flatten.Polygon();
    p.addFace(face.vertices.map(v => new Flatten.Point(v)));
    return p;
}

export const faceToPoly = moize(convertFaceToPoly);
