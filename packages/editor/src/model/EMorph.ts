/**
 * @paper-doll/editor
 * Copyright © 2021 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { nanoid } from "nanoid";
import { Hidable, Identifiable, Nameable } from "./BaseInterfaces";
import { ECage } from "./ECage";
import { EMorphKey, newMorphKey } from "./EMorphKey";

/**
 * Editable morph target
 */
export interface EMorph extends Identifiable, Nameable, Hidable {
    /**
     * Id of cages affected by the morph
     */
    cages: Array<string>;

    /**
     * Morph target keys per cage
     */
    keys: Record<number, EMorphKey>;
}

/**
 * Creates new morph for given cage
 * @param cageId - Id of the cage to govern the morph
 * @param name - Optional name of the cage
 */
export function newMorph(cage: ECage, name?: string): EMorph {

    const id = nanoid();
    const keyId = 100;

    const keyValue = newMorphKey(keyId, cage);

    return {
        id,
        name: name ?? `Morph ${id}`,
        cages: [cage.id],
        keys: {
            [keyId]: keyValue,
        },
        hidden: false,
    };
}