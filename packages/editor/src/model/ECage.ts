/*
 * @paper-doll/editor
 * Copyright © 2021 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { RectangleSize, Vertex } from "@paper-doll/core";
import { nanoid } from "nanoid";
import { Identifiable, Nameable, Hidable } from "./BaseInterfaces";
import { EFace, newFace, faceToPoly } from "./EFace";
import Flatten from "@flatten-js/core";

/**
 * Cage representation in the editor
 */
export interface ECage extends Identifiable, Nameable, Hidable {
    /**
     * Color of the cage in UI
     */
    color: string;

    /**
     * Cage faces
     */
    faces: Record<string, EFace>;
}

/**
 * Creates new cage
 * @param name - Optional name of the cage
 * @param color - Optional color of the cage
 */
export function newCage(dollSize: RectangleSize, name?: string, color: string = "black"): ECage {

    const id = nanoid();
    const face = newFace(dollSize);

    return {
        id,
        name: name ?? `Cage ${id}`,
        color,
        faces: {
            [face.id]: face
        },
        hidden: false,
    };
}

/**
 * Finds face that is closest to the given vertex
 * @param cage
 * @param vertex
 */
export function findFaceId(cage: ECage, vertex: Vertex): string {
    const p = new Flatten.Point(vertex[0], vertex[1]);
    const ids = Object.keys(cage.faces);
    const polygons = Object.values(cage.faces)
        .map(f => faceToPoly(f));

    let index = polygons.findIndex(q => q.contains(p));
    if (index >= 0) return ids[index];

    // Vertex is outside the cage - Try to assign nearest face
    const distances = polygons.map(q => q.distanceTo(p)[0]);
    const minDistance = Math.min(...distances);
    index = distances.findIndex(d => d === minDistance);
    return ids[index];
}