/**
 * @paper-doll/editor
 * Copyright © 2021 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { RectangleSize } from "@paper-doll/core";
import { Identifiable, Nameable } from "./BaseInterfaces";
import { Container } from "./Container";
import { ECage } from "./ECage";
import { EMorph } from "./EMorph";
import { EPart } from "./EPart";

/**
 * Editable paper doll
 */
export interface EDoll extends Identifiable, Nameable, RectangleSize {
    /**
     * Cages
     */
    cages: Container<ECage>;

    /**
     * Morph targets
     */
    morphs: Container<EMorph>;

    /**
     * Doll parts
     */
    parts: Container<EPart>;

    /**
     * Active morphs.
     * Id determines morph, value the morph coefficient
     */
    activeMorphs: Record<string, number>;
}
