/**
 * @paper-doll/editor
 * Copyright © 2021 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { nanoid } from "nanoid";
import { Hidable, Identifiable, Nameable } from "./BaseInterfaces";
import { Container } from "./Container";
import { EMesh } from "./EMesh";

/**
 * Editable doll part
 */
export interface EPart extends Identifiable, Nameable, Hidable {
    /**
     * Meshes used on background in order of drawing
     */
    background: Container<EMesh>;

    /**
     * Meshes used on foreground in order of drawing
     */
    foreground: Container<EMesh>;
}

/**
 * Creates new doll part
 * @returns
 */
export function newPart(): EPart {
    return {
        id: nanoid(),
        name: "New part",
        background: {
            id: "B",
            items: {},
            hidden: false,
        },
        foreground: {
            id: "F",
            items: {},
            hidden: false,
        },
        hidden: false,
    };
}