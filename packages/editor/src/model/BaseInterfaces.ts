/**
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

/**
 * Interface for objects that can be hidden
 */
export interface Hidable {
    /**
     * Should this object and all its children hidden?
     */
    hidden: boolean;
}

/**
 * Interface for objects with string Id
 */
export interface Identifiable {
    /**
     * Id of the object
     */
    id: string;
}

/**
 * Interface for objects with name
 */
export interface Nameable {
    /**
     * Name of the object
     */
    name: string;
}

