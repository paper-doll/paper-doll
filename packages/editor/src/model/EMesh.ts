/**
 * @paper-doll/editor
 * Copyright © 2021 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Vertex } from "@paper-doll/core";
import { nanoid } from "nanoid";
import { Hidable, Identifiable, Nameable } from "./BaseInterfaces";

/**
 * Mesh vertex bound to cage face
 */
export interface EBoundVertex {
    /**
     * Id of the face the vertex is bound to.
     * Null if the vertex is not bound
     */
    faceId?: string;

    /**
     * Vertex position
     */
    vertex: Vertex;
}

/**
 * Element of mesh
 */
export interface EMeshElement {
    /**
     * Type of mesh element
     */
    type: string;

    /**
     * Bound vertices
     */
    vertices: Array<EBoundVertex>;
}

/**
 * Editable mesh
 */
export interface EMesh<TType extends string = string> extends Identifiable, Nameable, Hidable {
    /**
     * Type of the mesh
     */
    type: TType;

    /**
     * Governing cage
     */
    cageId: string;

    /**
     * Mesh elements helps mesh decide how to draw the vertices
     */
    elements: Array<EMeshElement>;

    /**
     * Additional mesh properties
     */
    props?: unknown;
}
