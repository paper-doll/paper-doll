/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { StyledEngineProvider, ThemeProvider } from "@mui/material/styles";
import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import EditorApp from "./components/EditorApp";
import store from "./store";
import theme from "./theme";

const rootElement = document.getElementById("root");
ReactDOM.render(
    <Provider store={store}>
        <StyledEngineProvider injectFirst>
            <ThemeProvider theme={theme}>
                <EditorApp />
            </ThemeProvider>
        </StyledEngineProvider>
    </Provider>
    ,
    rootElement
);
