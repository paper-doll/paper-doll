/**
 * @paper-doll/editor
 * Copyright © 2021 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { AnyAction } from "redux";
import { EditorState, initialState } from "./model/EditorState";

/**
 * Editor reducer
 */
interface EditorReducer {
    (state: EditorState, action: AnyAction): EditorState;
};

/**
 * Editor reducer
 */
export interface TypedEditorReducer<TAction extends AnyAction> {
    (state: EditorState, action: TAction): EditorState;
}

/**
 * Action dictionary
 */
const actionReducers: Record<string, EditorReducer> = {};

/**
 * Registers action handler to reducer
 * @param type
 * @param handler
 */
export function registerAction<TAction extends AnyAction>(
    type: string,
    handler: TypedEditorReducer<TAction>
): void {

    if (actionReducers[type] == undefined) {
        actionReducers[type] = (s, a) => handler(s, a as TAction);
    }
    else {
        throw new Error(`Handler for action ${type} is already registered`);
    }
}

/**
 * Main reducer
 * @param state
 * @param action
 */
export default function rootReducer(state: EditorState = initialState, action: AnyAction): EditorState {
    try {
        var reducer = actionReducers[action.type];
        if (reducer == undefined) return state;
        return reducer(state, action);
    }
    catch (e: unknown) {
        if (e instanceof Error) {
            return {
                ...state,
                error: e.message
            };
        }
        else {
            return {
                ...state,
                error: JSON.stringify(e)
            };
        }
    }
}