/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import React from "react";
import { EFace } from "../../model";
import RecordItemSelect, { RecordItemSelectProps } from "../editors/RecordItemSelect";

/**
 * Face select properties
 */
export type FaceSelectProps = Omit<RecordItemSelectProps<EFace>, "label">;

/**
 * Component for selecting one face
 *
 * @param props
 * @returns
 */
export default function FaceSelect(props: FaceSelectProps) {
    return (
        <RecordItemSelect {...props} label="Faces" />
    );
}
