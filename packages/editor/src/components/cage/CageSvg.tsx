/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import React from "react";
import { ECage } from "../../model";
import { Polygon, VertexPoint } from "../svg";

/**
 * Properties for the CageSvg element
 */
export interface CageSvgProps {
    /**
     * Cage to show
     */
    cage: ECage;
    /**
     * Selection string
     */
    selection: string;
}

/**
 * Component for drawing cage on SVG
 * @param props
 * @returns
 */
export default function CageSvg(props: CageSvgProps) {

    const color = props.cage.color;

    const handle = `C:${props.cage.id}`;

    const isSelected = props.selection.startsWith(handle);

    // Faces
    const faces = Object.values(props.cage.faces).map(f => {

        const vertices = f.vertices.map((v, i) => {
            const vertexHandle = `${handle}:${f.id}:${i}`;
            return (
                <VertexPoint
                    key={`${f.id}:${i}`}
                    handle={vertexHandle}
                    v={v}
                    selected={vertexHandle === props.selection}
                />);
        });

        return (
            <g
                key={f.id}
                data-doll-handle={`${handle}:${f.id}`}
            >
                <Polygon
                    vertices={f.vertices}
                    stroke={color}
                    fill="transparent"
                    pointerEvents="visibleStroke"
                />
                {isSelected ? vertices : null}
            </g>
        );
    });

    return (
        <g data-doll-handle={handle}>
            {faces}
        </g>
    );
}
