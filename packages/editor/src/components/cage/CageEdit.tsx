/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Card, CardActions, CardContent, CardHeader } from "@mui/material";
import React from "react";
import { useDispatch } from "react-redux";
import { addCageFace, deleteCage, setCageColor, setPartName } from "../../actions";
import { useEditorState } from "../../model";
import AddButton from "../editors/AddButton";
import ColorButton from '../editors/ColorButton';
import DeleteButton from "../editors/DeleteButton";
import TextEdit from "../editors/TextEdit";

/**
 * Cage editor properties
 */
export interface CageEditProps {
    /** Id of edited cage */
    cageId: string;
}

/**
 * Cage properties editor to use in tree item
 * @param props
 * @returns
 */
export default function CageEdit(props: CageEditProps) {

    const dispatch = useDispatch();

    const cage = useEditorState(s => s.doll.cages.items[props.cageId]);

    const handleColorChange = (c: string) => {
        dispatch(setCageColor(cage.id, c));
    };

    const handleNameChange = (value: string) => {
        dispatch(setPartName(cage.id, value));
    };

    const handleCageDelete = () => dispatch(deleteCage(cage.id));

    const handleFaceAdd = () => dispatch(addCageFace(cage.id));

    return (
        <Card>
            <CardHeader title="Cage" subheader={cage.id} />
            <CardContent>
                <TextEdit
                    value={cage.name}
                    onValueChange={handleNameChange}
                />
                <ColorButton
                    color={cage.color}
                    size="small"
                    onChange={handleColorChange}
                />
            </CardContent>
            <CardActions>
                <AddButton onClick={handleFaceAdd}>
                    Add face
                </AddButton>
                <DeleteButton onClick={handleCageDelete}>
                    Delete cage
                </DeleteButton>
            </CardActions>
        </Card>
    );
}