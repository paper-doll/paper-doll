/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import React from "react";
import { ECage } from "../../model";
import RecordItemSelect, { RecordItemSelectProps } from "../editors/RecordItemSelect";

/**
 * Cage select properties
 */
export type CageSelectProps = Omit<RecordItemSelectProps<ECage>, "label">;

/**
 * Component for selecting one cage
 *
 * @param props
 * @returns
 */
export default function CageSelect(props: CageSelectProps) {
    return (
        <RecordItemSelect {...props} label="Cages" />
    );
}
