/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import AddCircleIcon from '@mui/icons-material/AddCircle';
import DeleteIcon from '@mui/icons-material/Delete';
import { Card, CardActions, CardContent, CardHeader, Stack, Typography } from "@mui/material";
import { Vertex } from "@paper-doll/core";
import React from "react";
import { useDispatch } from "react-redux";
import {
    addCageFaceVertex,
    deleteCageFace, deleteCageFaceVertex, select, setCageFaceVertex, setFaceName
} from "../../actions";
import { useEditorState } from "../../model";
import AddButton from '../editors/AddButton';
import DeleteButton from '../editors/DeleteButton';
import TextEdit from "../editors/TextEdit";
import VertexEdit from '../editors/VertexEdit';

/**
 * Cage face editor properties
 */
export interface CageFaceEditProps {
    /** If of the cage */
    cageId: string;
    /** Id of the face */
    faceId: string;
    /** Index of vertex */
    vertexId: number
}

/**
 * Cage face properties editor to use in tree item
 * @param props
 * @returns
 */
export default function CageFaceEdit(props: CageFaceEditProps) {

    const dispatch = useDispatch();

    const face = useEditorState(s => s.doll.cages.items[props.cageId].faces[props.faceId]);

    const handleNameChange = (value: string) => {
        dispatch(setFaceName(props.cageId, face.id, value));
    }

    const handleFaceDelete = () => dispatch(deleteCageFace(props.cageId, props.faceId));

    const handleVertexSelect = (vertexIndex: number) => {
        if (props.vertexId === vertexIndex) return;
        dispatch(select({
            type: "C",
            cageId: props.cageId,
            faceId: props.faceId,
            vertexId: vertexIndex
        }));
    };

    const handleVertexChange = (vertexIndex: number, value: Vertex) => dispatch(setCageFaceVertex(
        props.cageId,
        props.faceId,
        vertexIndex,
        value
    ));

    const handleVertexDelete = (vertexIndex: number) => dispatch(deleteCageFaceVertex(
        props.cageId,
        props.faceId,
        vertexIndex
    ));

    const handleVertexAdd = (vertexIndex: number) => dispatch(addCageFaceVertex(
        props.cageId,
        props.faceId,
        vertexIndex
    ));

    const vertices = face.vertices.map((v, i) => (
        <Stack
            key={i}
            direction="row"
            onClickCapture={() => handleVertexSelect(i)}
            sx={{
                backgroundColor: (i === props.vertexId)
                    ? "secondary.light"
                    : "",
                paddingTop: 0.25,
                paddingBottom: 0.25
            }}
        >
            <AddCircleIcon
                color="primary"
                fontSize="small"
                onClick={() => handleVertexAdd(i)}
            />
            <VertexEdit
                value={v}
                onValueChange={v => handleVertexChange(i, v)}
            />
            <DeleteIcon
                color="error"
                fontSize="small"
                onClick={() => handleVertexDelete(i)} />
        </Stack>
    ));

    return (
        <Stack spacing={1}>
            <Card>
                <CardHeader title="Cage Face" subheader={face.id} />
                <CardContent>
                    <TextEdit
                        value={face.name}
                        onValueChange={handleNameChange}
                    />
                </CardContent>
                <CardActions>
                    <DeleteButton onClick={handleFaceDelete}>
                        Delete face
                    </DeleteButton>
                </CardActions>
            </Card>
            <Card>
                <CardHeader title="Vertices" />
                <CardContent>
                    {vertices}
                </CardContent>
                <CardActions>
                    <AddButton onClick={() => handleVertexAdd(face.vertices.length)}>
                        Add vertex
                    </AddButton>
                </CardActions>
            </Card >
        </Stack >
    );
}