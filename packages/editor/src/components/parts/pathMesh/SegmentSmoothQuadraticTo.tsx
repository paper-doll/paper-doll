/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Stack, Typography } from "@mui/material";
import { Vertex } from "@paper-doll/core";
import React from "react";
import { useDispatch } from "react-redux";
import { setMeshSegmentVertex, setMeshSegmentVertexFace } from "../../../actions/parts";
import BoundVertexEdit from "../BoundVertexEdit";
import { PathSegmentEditProps } from "./PathSegmentEditProps";

const vertexId = 2;

/**
 * "Line to" segment
 * @param props
 */
export default function SegmentSmoothQuadraticTo(props: PathSegmentEditProps) {

    const dispatch = useDispatch();

    const handleFaceChange = (faceId: string | undefined) => {
        dispatch(setMeshSegmentVertexFace(
            {
                ...props.selection,
                vertexId
            },
            faceId
        ));
    };

    const handleVertexChange = (vertex: Vertex) => {
        dispatch(setMeshSegmentVertex(
            {
                ...props.selection,
                vertexId
            },
            vertex
        ));
    };

    return (
        <Stack>
            <Typography variant="caption">Smooth quadratic to</Typography>
            <BoundVertexEdit
                value={props.segment.vertices[vertexId]}
                faces={props.faces}
                onValueChange={handleVertexChange}
                onFaceChange={handleFaceChange}
            />
        </Stack>
    );
}