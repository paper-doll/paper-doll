/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Stack, Typography } from "@mui/material";
import { Vertex } from "@paper-doll/core";
import React from "react";
import { useDispatch } from "react-redux";
import { setMeshSegmentVertex, setMeshSegmentVertexFace } from "../../../actions/parts";
import BoundVertexEdit from "../BoundVertexEdit";
import { PathSegmentEditProps } from "./PathSegmentEditProps";

const inId = 0;
const outId = 1;
const toId = 2;

/**
 * "Cubic to" segment
 * @param props
 */
export default function SegmentCubicTo(props: PathSegmentEditProps) {

    const dispatch = useDispatch();

    const handleFaceChange = (vertexId: number, faceId: string | undefined) => {
        dispatch(setMeshSegmentVertexFace(
            {
                ...props.selection,
                vertexId
            },
            faceId
        ));
    };

    const handleVertexChange = (vertexId: number, vertex: Vertex) => {
        dispatch(setMeshSegmentVertex(
            {
                ...props.selection,
                vertexId
            },
            vertex
        ));
    };

    return (
        <Stack>
            <Typography variant="caption">Cubic to</Typography>
            <BoundVertexEdit
                value={props.segment.vertices[toId]}
                faces={props.faces}
                onValueChange={v => handleVertexChange(toId, v)}
                onFaceChange={f => handleFaceChange(toId, f)}
            />
            <Typography variant="caption">Control In</Typography>
            <BoundVertexEdit
                value={props.segment.vertices[inId]}
                faces={props.faces}
                onValueChange={v => handleVertexChange(inId, v)}
                onFaceChange={f => handleFaceChange(inId, f)}
            />
            <Typography variant="caption">Control out</Typography>
            <BoundVertexEdit
                value={props.segment.vertices[outId]}
                faces={props.faces}
                onValueChange={v => handleVertexChange(outId, v)}
                onFaceChange={f => handleFaceChange(outId, f)}
            />
        </Stack>
    );
}