/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import React from "react";
import { EPathMesh } from "../../../model";
import { VertexLine, VertexPoint } from "../../svg";

/**
 * Properties for the MeshSvg element
 */
export interface PathMeshSvgProps {
    /**
     * Mesh to show
     */
    mesh: EPathMesh;
    /**
     * Selection string
     */
    selection: string;
    /**
     * Parent handle
     */
    parentHandle: string;
}

/**
 * Component for drawing cage on SVG
 * @param props
 * @returns
 */
export default function PathMeshSvg(props: PathMeshSvgProps) {

    const pathText = props.mesh.elements
        .map(e => {
            const c1 = e.vertices[0].vertex;
            const c2 = e.vertices[1].vertex;
            const v = e.vertices[2].vertex;
            switch (e.type) {
                case "M":
                    return `M${v[0]},${v[1]}`;
                case "L":
                    return `L${v[0]},${v[1]}`;
                case "C":
                    return `C${c1[0]},${c1[1]} ${c2[0]},${c2[1]} ${v[0]},${v[1]}`;
                case "S":
                    return `S${c2[0]},${c2[1]} ${v[0]},${v[1]}`;
                case "Q":
                    return `Q${c2[0]},${c2[1]} ${v[0]},${v[1]}`;
                case "T":
                    return `T${v[0]},${v[1]}`;
                case "Z":
                    return "Z";
                default:
                    return ""
            }
        })
        .join(" ");

    const meshHandle = `${props.parentHandle}:${props.mesh.id}`

    const isSelected = props.selection.startsWith(meshHandle);

    const getVertices = () => props.mesh.elements
        .map((e, i, a) => {
            const c1 = e.vertices[0].vertex;
            const c2 = e.vertices[1].vertex;
            const v = e.vertices[2].vertex;

            const pi = i === 0
                ? a.length - 1
                : i - 1;
            const vp = a[pi].vertices[2].vertex;

            const segmentHandle = `${meshHandle}:${i}`;
            const handleC1 = `${segmentHandle}:0`;
            const handleC2 = `${segmentHandle}:1`;
            const handleV = `${segmentHandle}:2`;

            switch (e.type) {
                case "M":
                case "L":
                case "T":
                    return [
                        <VertexPoint
                            key={handleV}
                            handle={handleV}
                            v={v}
                            selected={handleV === props.selection}
                        />
                    ];
                case "C":
                    return [
                        <VertexLine v1={c1} v2={vp} />,
                        <VertexLine v1={c2} v2={v} />,
                        <VertexPoint
                            key={handleC1}
                            handle={handleC1}
                            v={c1}
                            selected={handleC1 === props.selection}
                        />,
                        <VertexPoint
                            key={handleC2}
                            handle={handleC2}
                            v={c2}
                            selected={handleC2 === props.selection}
                        />,
                        <VertexPoint
                            key={handleV}
                            handle={handleV}
                            v={v}
                            selected={handleV === props.selection}
                        />
                    ];
                case "S":
                case "Q":
                    return [
                        <VertexLine v1={c2} v2={v} />,
                        <VertexPoint
                            key={handleC2}
                            handle={handleC2}
                            v={c2}
                            selected={handleC2 === props.selection}
                        />,
                        <VertexPoint
                            key={handleV}
                            handle={handleV}
                            v={v}
                            selected={handleV === props.selection}
                        />
                    ];
                default:
                    return [];
            }
        })
        .flat();

    return ( // TODO: data-doll-handle={`P:${props.cage.id}`}
        <g data-doll-handle={meshHandle}>
            <path d={pathText} stroke={props.mesh.props.color} fill={props.mesh.props.fillColor} />
            {isSelected ? getVertices() : null}
        </g>
    );
}
