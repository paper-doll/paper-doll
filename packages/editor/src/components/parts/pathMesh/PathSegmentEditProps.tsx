import { MeshSegmentSelection } from '../../../actions/parts';
import { EFace, EPathMeshElement } from "../../../model";

/**
 * Properties for PathSegmentEdit
 */

export interface PathSegmentEditProps {
    /** Segment id */
    selection: MeshSegmentSelection;
    /** Segment data */
    segment: EPathMeshElement;
    /** Cage faces to bind to */
    faces: Record<string, EFace>;
    /** Is selected? */
    selected: boolean;
}
