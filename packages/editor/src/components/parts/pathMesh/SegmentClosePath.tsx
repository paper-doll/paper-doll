/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Stack, Typography } from "@mui/material";
import React from "react";

/**
 * "Close path" segment
 * @param props
 */
export default function SegmentClosePath() {
    return (
        <Stack>
            <Typography variant="caption">Close path</Typography>
        </Stack>
    );
}