/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import AddCircleIcon from '@mui/icons-material/AddCircle';
import DeleteIcon from '@mui/icons-material/Delete';
import { Stack } from "@mui/material";
import { PathSegmentType } from '@paper-doll/core';
import React from "react";
import { useDispatch } from 'react-redux';
import { addPathMeshSegment, deleteMeshSegment, setPathMeshSegmentType } from '../../../actions/parts';
import { select } from '../../../actions/Select';
import { PathSegmentEditProps } from './PathSegmentEditProps';
import { PathSegmentSelect } from './PathSegmentSelect';
import SegmentClosePath from './SegmentClosePath';
import SegmentCubicTo from './SegmentCubicTo';
import SegmentLineTo from './SegmentLineTo';
import SegmentMoveTo from './SegmentMoveTo';
import SegmentQuadraticTo from './SegmentQuadraticTo';
import SegmentSmoothCubicTo from './SegmentSmoothCubicTo';
import SegmentSmoothQuadraticTo from './SegmentSmoothQuadraticTo';

/**
 * Editor for EPathMesh segment
 * @param props
 */
export function PathSegmentEdit(props: PathSegmentEditProps) {

    const dispatch = useDispatch();

    const onSegmentAdd = () => dispatch(
        addPathMeshSegment(props.selection, props.segment.type)
    );

    const onSegmentSelect = () =>
        dispatch(select({
            type: "P",
            ...props.selection
        }));

    const onSegmentDelete = () => dispatch(
        deleteMeshSegment(props.selection)
    );

    const onSegmentTypeChange = (type: PathSegmentType) =>
        dispatch(setPathMeshSegmentType(props.selection, type))
        ;

    return (
        <Stack
            direction="row"
            onClickCapture={onSegmentSelect}
            sx={{
                backgroundColor: (props.selected)
                    ? "secondary.light"
                    : "",
                paddingTop: 0.25,
                paddingBottom: 0.25
            }}
        >
            <AddCircleIcon
                color="primary"
                fontSize="small"
                onClick={onSegmentAdd}
            />
            <PathSegmentSelect
                value={props.segment.type}
                onSelect={onSegmentTypeChange}
            />
            <SegmentEdit {...props} />
            <DeleteIcon
                color="error"
                fontSize="small"
                onClick={onSegmentDelete} />
        </Stack>
    );
}

function SegmentEdit(props: PathSegmentEditProps) {
    switch (props.segment.type) {
        case "M":
            return <SegmentMoveTo {...props} />;
        case "L":
            return <SegmentLineTo {...props} />;
        case "C":
            return <SegmentCubicTo {...props} />;
        case "S":
            return <SegmentSmoothCubicTo {...props} />;
        case "Q":
            return <SegmentQuadraticTo {...props} />;
        case "T":
            return <SegmentSmoothQuadraticTo {...props} />;
        case "Z":
            return <SegmentClosePath />;
        default:
            throw new Error(`Unknown path segment type ${props.segment.type}`);
    }
}
