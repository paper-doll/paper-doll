/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { MenuItem, Select, SelectChangeEvent } from "@mui/material";
import { PathSegmentType } from "@paper-doll/core";
import React from "react";

/**
 * Properties for PathSegmentSelect component
 */
export interface PathSegmentSelectProps {
    /**
     * Value
     */
    value: PathSegmentType;

    /**
     * Event raised when segment is selected
     */
    onSelect: (value: PathSegmentType) => void;
}

/**
 * Select for path mesh segment
 * @param props
 * @returns
 */
export function PathSegmentSelect(props: PathSegmentSelectProps) {

    const handleChange = (e: SelectChangeEvent) => {
        const v = e.target.value as PathSegmentType;
        props.onSelect?.(v);
    }

    return (
        <Select
            variant="standard"
            value={props.value}
            label="Segment Type"
            onChange={handleChange}
            sx={{minWidth: "10em"}}
        >
            <MenuItem value="M">Move to</MenuItem>
            <MenuItem value="L">Line to</MenuItem>
            <MenuItem value="C">Cubic</MenuItem>
            <MenuItem value="S">Cubic smooth</MenuItem>
            <MenuItem value="Q">Quadratic</MenuItem>
            <MenuItem value="T">Quadratic smooth</MenuItem>
            <MenuItem value="Z">Close path</MenuItem>
        </Select>
    );
}