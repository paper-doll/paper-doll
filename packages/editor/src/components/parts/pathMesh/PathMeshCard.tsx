/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Card, CardActions, CardContent, CardHeader, Stack, Typography } from "@mui/material";
import React from "react";
import { useDispatch } from "react-redux";
import { addPathMeshSegment, setPathMeshColor, setPathMeshFillColor } from "../../../actions/parts";
import { EPathMeshElement, isEPathMesh, PartSelection, PathMeshType, useEditorState } from "../../../model";
import AddButton from "../../editors/AddButton";
import ColorButton from "../../editors/ColorButton";
import { MeshCardProps } from "../MeshCardProps";
import { PathSegmentEdit } from "./PathSegmentEdit";

/**
 * Path mesh card
 * @param mesh
 * @returns
 */
export function PathMeshCard(props: MeshCardProps) {

    if (!isEPathMesh(props.mesh)) {
        throw new Error(`Mesh ${props.mesh.id} is not Path Mesh!`);
    }

    const dispatch = useDispatch();

    const handleSegmentAdd = () => {
        dispatch(addPathMeshSegment(
            {
                ...props.selection,
                elementId: props.mesh.elements.length
            },
            "L"
        ));
    };

    const handleColorChange = (color : string) => {
        dispatch(setPathMeshColor(
            props.selection,
            color
        ));
    };

    const handleFillColorChange = (color : string) => {
        dispatch(setPathMeshFillColor(
            props.selection,
            color
        ));
    };

    const selectedSegment = useEditorState(
        s => (s.selection as PartSelection).elementId
    );

    const segments = props.mesh.elements.map((e, i) =>
        <PathSegmentEdit
            key={i}
            selection={{ ...props.selection, elementId: i }}
            segment={e as EPathMeshElement}
            faces={props.faces}
            selected={i === selectedSegment}
        />
    );

    return (
        <>
            <Card>
                <CardHeader title="Stroke and fill" />
                <CardContent>
                    <Stack direction="row" spacing={1} >
                        <ColorButton
                            color={props.mesh.props.color}
                            size="small"
                            onChange={handleColorChange}
                        />
                        <ColorButton
                            color={props.mesh.props.fillColor}
                            size="small"
                            onChange={handleFillColorChange}
                        />
                    </Stack>
                </CardContent>
            </Card>
            <Card>
                <CardHeader title="Segments" />
                <CardContent>
                    {segments}
                </CardContent>
                <CardActions>
                    <AddButton onClick={handleSegmentAdd}>
                        Add segment
                    </AddButton>
                </CardActions>
            </Card>
        </>
    );
}
