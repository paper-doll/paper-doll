/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { MeshSelection } from "../../actions/parts";
import { EFace, EMesh } from "../../model";

/**
 * Properties for mesh card
 */
export interface MeshCardProps {
    /** Mesh selection to display / edit */
    selection: MeshSelection;
    /** Mesh data */
    mesh: EMesh;
    /** Faces the mesh can bind to */
    faces: Record<string, EFace>;
}
