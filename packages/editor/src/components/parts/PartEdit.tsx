/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Card, CardActions, CardContent, CardHeader, InputLabel, Stack } from "@mui/material";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { addBackgroundMesh, addForegroundMesh, deletePart, setPartName } from "../../actions";
import { meshTypeNames, useEditorState } from "../../model";
import CageSelect from "../cage/CageSelect";
import AddButton from "../editors/AddButton";
import DeleteButton from "../editors/DeleteButton";
import TextEdit from "../editors/TextEdit";
import MeshTypeSelect from "./MeshTypeSelect";

/**
 * Cage editor properties
 */
export interface PartsEditProps {
    /** Id of edited part */
    partId: string;
}

/**
 * Cage properties editor to use in tree item
 * @param props
 * @returns
 */
export default function CageEdit(props: PartsEditProps) {

    const dispatch = useDispatch();

    const cages = useEditorState(s => s.doll.cages.items);
    const part = useEditorState(s => s.doll.parts.items[props.partId]);

    const handleNameChange = (value: string) => {
        dispatch(setPartName(part.id, value));
    };

    const [cageId, setCageId] = useState(Object.keys(cages)[0]);
    const [meshType, setMeshType] = useState(Object.keys(meshTypeNames)[0]);

    const handleCageSelect = (id?: string) => setCageId(id!);
    const handleMeshTypeSelect = (id: string) => setMeshType(id);

    const handlePartDelete = () => dispatch(deletePart(part.id));

    const handleForegroundAdd = () => dispatch(addForegroundMesh(part.id, meshType, cageId!));
    const handleBackgroundAdd = () => dispatch(addBackgroundMesh(part.id, meshType, cageId!));

    const disableAddMesh = cageId === null || meshType === null;

    return (
        <Stack spacing={1}>
        <Card>
            <CardHeader title="Part" subheader={part.id} />
            <CardContent>
                <TextEdit
                    value={part.name}
                    onValueChange={handleNameChange}
                />
            </CardContent>
            <CardActions>
                <DeleteButton onClick={handlePartDelete}>
                    Delete part
                </DeleteButton>
            </CardActions>
        </Card>
        <Card>
            <CardHeader title="Meshes" />
            <CardContent>
                <InputLabel>Cage</InputLabel>
                <CageSelect items={cages} onSelect={handleCageSelect} />
                <InputLabel>Mesh type</InputLabel>
                <MeshTypeSelect onSelect={handleMeshTypeSelect} />
            </CardContent>
            <CardActions>
                <AddButton disabled={disableAddMesh} onClick={handleForegroundAdd}>
                    Add foreground mesh
                </AddButton>
                <AddButton disabled={disableAddMesh} onClick={handleBackgroundAdd}>
                    Add background mesh
                </AddButton>
            </CardActions>
        </Card>
        </Stack>
    );
}