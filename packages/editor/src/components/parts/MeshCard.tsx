/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Card, CardHeader } from "@mui/material";
import React from "react";
import { PathMeshType } from "../../model";
import { MeshCardProps } from "./MeshCardProps";
import { PathMeshCard } from "./pathMesh/PathMeshCard";

/**
 * Creates new mesh of given type
 * @param props - Mesh card props
 * @returns - Created mesh
 */
export default function MeshCard(props: MeshCardProps): JSX.Element {
    switch (props.mesh.type) {
        case PathMeshType:
            return <PathMeshCard {...props} />;
        default:
            return (
                <Card>
                    <CardHeader title={`Cannot create panel for unknown mesh type '${props.mesh.type}'`} />
                </Card>
            );
    }
}
