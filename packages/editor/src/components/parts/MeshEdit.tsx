/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Card, CardActions, CardContent, CardHeader, Stack } from "@mui/material";
import React from "react";
import { useDispatch } from "react-redux";
import { deleteMesh, MeshSelection, setMeshName } from "../../actions";
import { useEditorState } from "../../model";
import DeleteButton from "../editors/DeleteButton";
import TextEdit from "../editors/TextEdit";
import MeshCard from "./MeshCard";
import "./pathMesh/PathMeshCard";

/**
 * Mesh properties
 */
export interface MeshEditProps {
    /** Mesh selection */
    mesh: MeshSelection;
}

/**
 * Component for editing mesh
 * @param props
 */
export default function MeshEdit(props: MeshEditProps) {

    const dispatch = useDispatch();

    const mesh = useEditorState(
        s => props.mesh.placement === "F"
            ? s.doll.parts.items[props.mesh.partId].foreground.items[props.mesh.meshId]
            : s.doll.parts.items[props.mesh.partId].background.items[props.mesh.meshId]
    );

    const faces = useEditorState(s => s.doll.cages.items[mesh.cageId].faces);

    const handleNameChange = (value: string) => {
        dispatch(setMeshName(props.mesh, value));
    }

    const handleMeshDelete = () => dispatch(
        deleteMesh(props.mesh)
    );

    return (
        <Stack spacing={1}>
            <Card>
                <CardHeader title="Mesh" subheader={mesh.id} />
                <CardContent>
                    <TextEdit
                        value={mesh.name}
                        onValueChange={handleNameChange}
                    />
                </CardContent>
                <CardActions>
                    <DeleteButton onClick={handleMeshDelete}>
                        Delete mesh
                    </DeleteButton>
                </CardActions>
            </Card>
            <MeshCard selection={props.mesh} mesh={mesh} faces={faces} />
        </Stack>
    )
}