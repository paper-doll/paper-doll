/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { MenuItem, Select, SelectChangeEvent } from "@mui/material";
import React, { useState } from "react";
import { meshTypeNames } from "../../model";

/**
 * MeshType select properties
 */
export interface MeshTypeSelectProps {
    /**
     * Event raised when cage is selected
     */
    onSelect: (id: string) => void;
}

/**
 * Component for selecting one cage
 *
 * @param props
 * @returns
 */
export default function MeshTypeSelect(props: MeshTypeSelectProps) {

    const meshes = Object.entries(meshTypeNames);

    const [value, setValue] = useState(meshes[0][0]);

    const items = meshes.map(kv =>
        <MenuItem key={kv[0]} value={kv[0]}>{kv[1]}</MenuItem>
    );

    const handleChange = (e: SelectChangeEvent) => {
        const value = e.target.value;
        setValue(value)
        props.onSelect?.(value);
    }

    return (
        <Select
            variant="standard"
            value={value}
            label="Mesh Type"
            onChange={handleChange}>
            {items}
        </Select>
    );
}