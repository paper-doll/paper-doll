/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import React from "react";
import { EMesh, EPathMesh, PathMeshType } from "../../model";
import PathMeshSvg from "./pathMesh/PathMeshSvg";

/**
 * Properties for the MeshSvg element
 */
export interface MeshSvgProps {
    /**
     * Mesh to show
     */
    mesh: EMesh;
    /**
     * Selection string
     */
    selection: string;
    /**
     * Parent handle
     */
    parentHandle: string;
}

/**
 * Component for drawing cage on SVG
 * @param props
 * @returns
 */
export default function MeshSvg(props: MeshSvgProps) {

    switch(props.mesh.type) {
        case PathMeshType:
            return <PathMeshSvg
                mesh={props.mesh as EPathMesh}
                selection={props.selection}
                parentHandle={props.parentHandle}
                />;
        default:
            return null;
    }
}
