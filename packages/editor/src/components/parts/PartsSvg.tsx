/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import React from "react";
import { EPart } from "../../model";
import MeshSvg from "./MeshSvg";

/**
 * Properties for the PartSvg element
 */
export interface PartsSvgProps {
    /**
     * Part to show
     */
    part: EPart;
    /**
     * Selection string
     */
    selection: string;
}

/**
 * Component for drawing part on SVG
 * @param props
 * @returns
 */
export default function PartSvg(props: PartsSvgProps) {

    const handle = `P:${props.part.id}`;

    const background = Object.values(props.part.background.items)
        .filter(m => !m.hidden)
        .map(m => <MeshSvg
            key={m.id}
            mesh={m}
            selection={props.selection}
            parentHandle={`${handle}:B`}
        />);

    const foreground = Object.values(props.part.foreground.items)
        .filter(m => !m.hidden)
        .map(m => <MeshSvg
            key={m.id}
            mesh={m}
            selection={props.selection}
            parentHandle={`${handle}:F`}
        />);

    return (
        <g data-doll-handle={handle}>
            {background}
            {foreground}
        </g>
    );
}
