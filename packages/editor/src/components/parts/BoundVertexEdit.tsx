/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Stack, SxProps } from "@mui/material";
import { Vertex } from "@paper-doll/core";
import React from "react";
import { EBoundVertex, EFace } from "../../model";
import CageFaceSelect from "../cage/CageFaceSelect";
import IntegerEdit from "../editors/IntegerEdit";

/**
 * VertexEdit properties
 */
export interface VertexEditProps {
    /**
     * Item value
     */
    value: EBoundVertex;
    /**
     * Bindable faces
     */
    faces: Record<string, EFace>;
    /**
     * Callback on value change
     */
    onValueChange?: (value: Vertex) => void;
    /**
     * Callback on binding face change
     */
    onFaceChange?: (faceId: string | undefined) => void;
}

/**
 * Vertex edit component
 *
 * @param props
 */
const VertexEdit: React.FC<VertexEditProps> = (props) => {

    const [x, y] = props.value.vertex;

    const handleChangeX = (value?: number) => props.onValueChange?.([value ?? 0, y]);
    const handleChangeY = (value?: number) => props.onValueChange?.([x, value ?? 0]);
    const handleFaceSelect = (id: string | undefined) => props.onFaceChange?.(id);

    const style : SxProps = {
        width: "3.5em",
        maxWidth:"3.5em",
    };

    return (
        <Stack direction="row" spacing={1}>
            <IntegerEdit
                required
                value={x}
                thousandSeparator
                onValueChange={handleChangeX}
                sx={style}
            />
            <IntegerEdit
                required
                value={y}
                thousandSeparator
                onValueChange={handleChangeY}
                sx={style}
            />
            <CageFaceSelect
                items={props.faces}
                value={props.value.faceId}
                allowNone={true}
                onSelect={handleFaceSelect}
                />
        </Stack>
    );
}

export default VertexEdit;