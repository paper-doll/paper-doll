/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Box, BoxProps } from "@mui/material";
import React from "react";

/**
 * Wraps component in a Box that prevents click propagation
 * @param props
 * @returns
 */
export default function StopClickPropagation(props: BoxProps) {
    return (
        <Box
            {...props}
            display="flex"
            onClick={(e) => e.stopPropagation()}
        />
    );
};
