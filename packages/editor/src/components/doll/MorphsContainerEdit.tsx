/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Card, CardActions, CardContent, CardHeader } from "@mui/material";
import React from "react";
import { useDispatch } from "react-redux";
// import { addMorph } from "../../actions";
import AddButton from "../editors/AddButton";

/**
 * Doll editor
 * @param props
 */
export default function MorphsContainerEdit() {

    const dispatch = useDispatch();

    const handleMorphAdd = () => {
        // dispatch(addMorph());
    }

    return (
        <Card>
            <CardHeader title="Morphs"/>
            <CardContent>
                Morphs define presets for cage distortions.
            </CardContent>
            <CardActions>
                <AddButton disabled onClick={handleMorphAdd}>
                    Add morph
                </AddButton>
            </CardActions>
        </Card>
    );
}