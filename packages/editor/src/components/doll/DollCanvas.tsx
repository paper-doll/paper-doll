/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Stack, Typography } from "@mui/material";
import { Vertex } from "@paper-doll/core";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { setCageFaceVertex, select, setMeshSegmentVertex } from "../../actions";
import { EDoll } from "../../model";
import CageSvg from "../cage/CageSvg";
import PartSvg from "../parts/PartsSvg";
import { Canvas } from "../svg";

/**
 * Properties for the doll canvas component
 */
export interface DollCanvasProps {
    /**
     * Window height
     */
    windowHeight: number;
    /**
     * Doll to show
     */
    doll: EDoll;
    /**
     * Selection string
     */
    selection: string;
}

/**
 * Canvas to display and edit doll
 * @param props
 * @returns
 */
export default function DollCanvas(props: DollCanvasProps) {

    const dispatch = useDispatch();

    let elements = [
        <rect
            key="canvas-background-rect"
            x={0}
            y={0}
            width={props.doll.width}
            height={props.doll.height}
            stroke="none"
            fill="white"
        />
    ];

    elements = elements.concat(
        Object.values(props.doll.parts.items)
            .filter(part => !part.hidden)
            .map(part =>
                <PartSvg
                    key={part.id}
                    part={part}
                    selection={props.selection}
                />
            )
    );

    elements = elements.concat(
        Object.values(props.doll.cages.items)
            .filter(cage => !cage.hidden)
            .map(cage =>
                <CageSvg
                    key={cage.id}
                    cage={cage}
                    selection={props.selection}
                />
            )
    );

    const handleElementSelect = (id: string) => {
        dispatch(select(id));
    };

    const handleHandleMove = (id: string, pos: Vertex, delta: Vertex) => {
        const parts = id.split(":");
        switch (parts[0]) {
            case "C":
                if (parts[3] !== undefined) {
                    dispatch(setCageFaceVertex(
                        parts[1],
                        parts[2],
                        parseInt(parts[3]),
                        pos
                    ));
                }
                break;
            case "P":
                if (parts[5] != undefined) {
                    dispatch(setMeshSegmentVertex(
                        {
                            partId: parts[1],
                            placement: parts[2] === "F" ? "F" : "B",
                            meshId: parts[3],
                            elementId: parseInt(parts[4]),
                            vertexId: parseInt(parts[5])
                        },
                        pos
                    ));
                }
        }
    };

    // Panning
    const [showPos, setShowPos] = useState([
        props.doll.width / 2,
        props.doll.height / 2
    ]);
    const handlePan = (pos: Vertex) => {
        setShowPos(pos);
    }

    // Zoom
    const [zoom, setZoom] = useState(100);
    const handleZoom = (zoomIn: boolean) => {
        const delta = zoomIn
            ? +10
            : -10;
        let newZoom = zoom + delta;
        if (newZoom < 20) newZoom = 20;
        if (newZoom > 400) newZoom = 400;
        setZoom(newZoom);
    }

    const height = Math.trunc(props.windowHeight * 0.85);

    console.log(`Canvas max height: ${height}`);

    return (
        <Stack>
            <Typography variant="caption">
                {`View Pos: ${showPos[0]}:${showPos[1]} Zoom: ${zoom}%`}
            </Typography>
            <Canvas
                canvasHeight={props.doll.height}
                canvasWidth={props.doll.width}
                showX={showPos[0]}
                showY={showPos[1]}
                zoom={zoom}
                sx={{
                    backgroundColor: "lightGray",
                    borderColor: "#bebebe",
                    borderStyle: "solid",
                    borderWidth: "2px",
                    height: height,
                }}
                onElementSelect={handleElementSelect}
                onElementMove={handleHandleMove}
                onPan={handlePan}
                onZoom={handleZoom}
            >
                {elements}
            </Canvas>
        </Stack>
    );
}