/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Card, CardActions, CardContent, CardHeader } from "@mui/material";
import React from "react";
import { useDispatch } from "react-redux";
import { addPart } from "../../actions";
import AddButton from "../editors/AddButton";

/**
 * Doll editor
 * @param props
 */
export default function PartsContainerEdit() {

    const dispatch = useDispatch();

    const handlePartAdd = () => {
        dispatch(addPart());
    }

    return (
        <Card>
            <CardHeader title="Doll Parts"/>
            <CardContent>
                The Doll consists of parts that can be drawn
                and morphed by cages and morphs.
                Each part consists of foreground and background meshes.
            </CardContent>
            <CardActions>
                <AddButton onClick={handlePartAdd}>
                    Add part
                </AddButton>
            </CardActions>
        </Card>
    );
}