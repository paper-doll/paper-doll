/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { TreeView } from "@mui/lab";
import React from "react";
import { Container, EDoll, EMesh } from "../../model";
import DollTreeItem from "./DollTreeItem";

/**
 * Properties for the DollTree component
 */
export interface DollTreeProps {
    /**
     * Doll tree source
     */
    doll: EDoll;

    /**
     * Maximal allowed height
     */
    maxHeight?: number;

    /**
     * On selection event
     */
    onSelect: (path: string) => void;
}

/**
 * Doll Tree component
 * @param props
 */
export default function DollTree(props: DollTreeProps) {

    const cages = getCagesTree(props.doll);
    const parts = getPartsTree(props.doll);
    const morphs = getMorphsTree(props.doll);

    const handleNodeSelect = (event: React.SyntheticEvent, value: string) => {
        if (value.startsWith("-")) return;
        props.onSelect?.(value);
    }

    return (
        <TreeView
            defaultCollapseIcon={<ExpandMoreIcon />}
            defaultExpandIcon={<ChevronRightIcon />}
            defaultExpanded={["D", "C"]}
            onNodeSelect={handleNodeSelect}
            sx={{
                overflow: "auto",
                maxHeight: props.maxHeight,
                flexGrow: 1
            }}
        >
            <DollTreeItem nodeId="D" label="Doll">
                <DollTreeItem nodeId="P" label="Parts">
                    {parts}
                </DollTreeItem>
                <DollTreeItem nodeId="C" label="Cages">
                    {cages}
                </DollTreeItem>
                <DollTreeItem nodeId="M" label="Morphs">
                    {morphs}
                </DollTreeItem>

            </DollTreeItem>
        </TreeView>
    );
}

function getCagesTree(doll: EDoll) {
    return Object.values(doll.cages.items).map(c => {
        const nodeId = `C:${c.id}`;
        const faces = Object.values(c.faces)
            .map(f => (
                <DollTreeItem
                    key={f.id}
                    nodeId={`${nodeId}:${f.id}`}
                    label={f.name}
                />
            ));
        return (
            <DollTreeItem
                key={c.id}
                nodeId={nodeId}
                label={c.name}
            >
                {faces}
            </DollTreeItem>
        );
    })
}

function getPartsTree(doll: EDoll) {
    // TODO: Fully implement
    return Object.values(doll.parts.items).map(p => {
        const nodeId = `P:${p.id}`;

        return (
            <DollTreeItem key={p.id} nodeId={nodeId} label={p.name}>
                {getMeshTree(nodeId, "Foreground", p.foreground)}
                {getMeshTree(nodeId, "Background", p.background)}
            </DollTreeItem>
        );
    })
}

function getMeshTree(parentId: string, label: string, meshContainer: Container<EMesh>) {

    const nodeId = `${parentId}:${meshContainer.id}`;

    const meshes = Object.keys(meshContainer.items).map(k => (
        <DollTreeItem
            key={k}
            nodeId={`${nodeId}:${k}`}
            label={meshContainer.items[k].name}
        />
    ));

    return (
        <DollTreeItem key={meshContainer.id} nodeId={nodeId} label={label}>
            {meshes}
        </DollTreeItem>
    );
}

function getMorphsTree(doll: EDoll) {
    return Object.values(doll.morphs.items).map(m => {
        const morphNodeId = `M:${m.id}`;
        const keys = Object.values(m.keys).map(k => {
            const keyNodeId = `${morphNodeId}:${k.value}`;
            const cages = Object.entries(k.cages).map(ckv => {
                const cage = doll.cages.items[ckv[0]];
                const cageNodeId = `${keyNodeId}:${cage.id}`;
                const faces = Object.values(cage.faces).map(f =>
                    <DollTreeItem
                        key={f.id}
                        nodeId={`${cageNodeId}:${f.id}`}
                        label={f.name}
                    />
                );
                return (
                    <DollTreeItem
                        key={cage.id}
                        nodeId={cageNodeId}
                        label={cage.name}
                    >
                        {faces}
                    </DollTreeItem>
                );
            });
            return (
                <DollTreeItem
                    key={k.value}
                    nodeId={keyNodeId}
                    label={k.value}
                >
                    {cages}
                </DollTreeItem>
            );
        });
        return (
            <DollTreeItem
                key={m.name}
                nodeId={morphNodeId}
                label={m.name}
            >
                {keys}
            </DollTreeItem>
        );
    })
}