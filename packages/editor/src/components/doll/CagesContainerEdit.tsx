/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Card, CardActions, CardContent, CardHeader } from "@mui/material";
import React from "react";
import { useDispatch } from "react-redux";
import { addCage } from "../../actions";
import AddButton from "../editors/AddButton";

/**
 * Doll editor
 * @param props
 */
export default function CagesContainerEdit() {

    const dispatch = useDispatch();

    const handleCageAdd = () => {
        dispatch(addCage());
    }

    return (
        <Card>
            <CardHeader title="Cages"/>
            <CardContent>
                Cages are used to define the boundaries in which
                the doll parts will be morphed
            </CardContent>
            <CardActions>
                <AddButton onClick={handleCageAdd}>
                    Add cage
                </AddButton>
            </CardActions>
        </Card>
    );
}