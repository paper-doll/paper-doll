/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Card, CardActions, CardContent, CardHeader } from "@mui/material";
import React from "react";
import { useDispatch } from "react-redux";
import { addCage, addPart, setDollName } from "../../actions";
import { useEditorState } from "../../model";
import AddButton from "../editors/AddButton";
import TextEdit from "../editors/TextEdit";

/**
 * Doll editor
 * @param props
 */
export default function DollEdit() {

    const dispatch = useDispatch();

    const dollId = useEditorState(s => s.doll.id);
    const dollName = useEditorState(s => s.doll.name);

    const handleNameChange = (value: string) => {
        dispatch(setDollName(value));
    }

    const handleCageAdd = () => {
        dispatch(addCage());
    }

    const handlePartAdd = () => {
        dispatch(addPart());
    }

    return (
        <Card>
            <CardHeader title="Paper Doll" subheader={dollId} />
            <CardContent>
                <TextEdit
                    value={dollName}
                    onValueChange={handleNameChange}
                />
            </CardContent>
            <CardActions>
                <AddButton onClick={handleCageAdd}>
                    Add cage
                </AddButton>
                <AddButton onClick={handlePartAdd}>
                    Add doll part
                </AddButton>
            </CardActions>
        </Card>
    );
}