/*
 * @paper-doll/editor
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import React from "react";
import { Vertex } from "@paper-doll/core";

/**
 * Polygon properties
 */
export interface PolygonProps extends Omit<React.SVGProps<SVGPolygonElement>, "points"> {
    /**
     * Points of the polygon
     */
    vertices: Vertex[];
}

/**
 * SVG Polygon
 *
 * @param props
 */
export default function Polygon(props: PolygonProps) {
    const { vertices, ...rest} = props;
    const points = vertices
        .map(v => `${v[0]},${v[1]}`)
        .join(" ");
    return (
        <polygon
            {...rest}
            points={points}
        />
    );
};
