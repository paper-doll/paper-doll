/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { styled, SxProps, Theme } from "@mui/material";
import { Vertex } from "@paper-doll/core";
import React, { useEffect, useRef, useState } from "react";

const StyledSvg = styled('svg')({});

/**
 * Canvas properties
 */
export interface CanvasProps {
    /**
     * Canvas drawing area width
     */
    canvasWidth: number;

    /**
     * Canvas drawing area height
     */
    canvasHeight: number;

    /**
     * X-coordinate in canvas space of point that should be shown
     * at the center of the view.
     * If not specified, center of canvas will be used
     */
    showX?: number;

    /**
     * Y-coordinate in canvas space of point that should be shown
     * at the center of the view
     * If not specified, center of canvas will be used
     */
    showY?: number;

    /**
     * Zoom level in percent.
     * If not specified, 100% is used.
     */
    zoom?: number;

    /**
     * Element select
     */
    onElementSelect?: (id: string) => void;

    /**
     * Handle move select
     */
    onElementMove?: (id: string, pos: Vertex, delta: Vertex) => void;

    /**
     * Canvas panned
     */
    onPan?: (pos: Vertex) => void;

    /**
     * Canvas zoomed
     */
    onZoom?: (zoomIn: boolean) => void;

    /**
     * Style property
     */
    sx?: SxProps<Theme>;

    /**
     * Children to display.
     * All draggable children must specify the `data-doll-handle` attribute 1
     * containing its selection path.
     */
    children?: JSX.Element | JSX.Element[];
}

/**
 * SVG "canvas"
 */
export const Canvas: React.FC<CanvasProps> = props => {

    // Self reference
    const ref = useRef<SVGSVGElement>(null);

    // State
    const [dragHandle, setDragHandle] = useState<string | null>(null);
    const [dragStart, setDragStart] = useState<[number, number]>([0, 0]);

    const [panStart, setPanStart] = useState<Vertex | null>(null);

    const [viewBox, setViewBox] = useState(`0 0 ${props.canvasWidth} ${props.canvasHeight}`);

    const showX = props.showX ?? props.canvasWidth / 2;
    const showY = props.showY ?? props.canvasHeight / 2;

    useEffect(
        () => {
            const componentWidth = ref.current?.clientWidth ?? props.canvasWidth;
            const componentHeight = ref.current?.clientHeight ?? props.canvasHeight;

            console.log(`Canvas client size: ${componentWidth}x${componentHeight}`);

            const zoom = (props.zoom ?? 100) / 100;

            const w = componentWidth / zoom;
            const h = componentHeight / zoom;

            const x = showX - w / 2;
            const y = showY - h / 2;

            setViewBox(`${x} ${y} ${w} ${h}`);
        },
        [ref, props.zoom, props.showX, props.showY]
    );

    return (
        <StyledSvg
            ref={ref}
            sx={props.sx}
            viewBox={viewBox}
            onMouseDown={mouseDown}
            onMouseMove={mouseMove}
            onMouseLeave={mouseUp}
            onMouseUp={mouseUp}
            onWheel={mouseWheel}
        >
            {props.children}
        </StyledSvg>
    );

    /**
     * Calculates mouse position in SVG coordinates
     * @param e
     */
    function getMousePosition(
        e: React.MouseEvent<SVGGraphicsElement>,
        clip: boolean = true
    ): [x: number, y: number] | null {

        const ctm = ref.current?.getScreenCTM();
        if (!ctm) return null;

        const mx = Math.round((e.clientX - ctm.e) / ctm.a);
        const my = Math.round((e.clientY - ctm.f) / ctm.d);
        if (!clip) return [mx, my];

        const x = Math.max(0, Math.min(props.canvasWidth, mx));
        const y = Math.max(0, Math.min(props.canvasHeight, my));
        return [x, y];
    }

    /**
     * Mouse button pressed
     * @param e
     */
    function mouseDown(e: React.MouseEvent<SVGGraphicsElement>) {
        switch (e.button) {
            case 0: // Main mouse button drags object
                startElementDrag(e);
                break;
            case 1: // Middle button pans
                startCanvasPan(e);
                break;
        }
    }

    /**
     * Mouse button released
     * @param e
     */
    function mouseUp() {
        if (dragHandle != null) {
            setDragHandle(null);
        }
        if (panStart !== null) {
            setPanStart(null);
        }
    }

    /**
     * Mouse wheel rotated
     * @param e
     */
    function mouseWheel(e: React.WheelEvent) {
        e.stopPropagation();
        props.onZoom?.(e.deltaY < 0);
    }

    /**
     * Move the handle to new position
     * @param e
     */
    function mouseMove(e: React.MouseEvent<SVGGraphicsElement>) {
        e.preventDefault();
        dragElement(e);
        panCanvas(e);
    }

    /**
     * Gets handle Id of the element clicked.
     * If not found on the direct element, looks up in the SVG DOM tree.
     *
     * @param element - HTML element
     * @returns Handle Id or null if not found
     */
    function getHandleId(element: HTMLElement | null): (string | null) {
        let e = element;
        while (e) {
            const handle = e.dataset.dollHandle;
            if (handle != undefined) return handle;
            e = e.parentElement;
        }
        return null;
    }

    function startElementDrag(e: React.MouseEvent<SVGGraphicsElement>): void {
        // Return if drag is already in progress
        if (dragHandle != null) return;

        const start = getMousePosition(e, true);
        if (start === null) return;

        // Enable drag
        const id = getHandleId(e.target as HTMLElement);
        if (id) {
            props.onElementSelect?.(id);
            setDragHandle(id);
            setDragStart(start);
        }
    }

    function dragElement(e: React.MouseEvent<SVGGraphicsElement>): void {
        if (dragHandle === null) return;

        const mousePosition = getMousePosition(e, true);
        if (mousePosition === null) return;

        const dx = mousePosition[0] - dragStart[0];
        const dy = mousePosition[1] - dragStart[1];

        if (dx === 0 && dy === 0) return;

        // Update the position of the handle if changed
        props.onElementMove?.(
            dragHandle,
            mousePosition,
            [dx, dy]
        );
        setDragStart(mousePosition);
    }

    function startCanvasPan(e: React.MouseEvent<SVGGraphicsElement>): void {
        // Return if pan already in progress
        if (panStart !== null) return;

        const start = getMousePosition(e, false);
        if (start === null) return;

        // Enable pan
        setPanStart(start);
    }

    function panCanvas(e: React.MouseEvent<SVGGraphicsElement>): void {
        if (panStart === null) return;

        const mousePosition = getMousePosition(e, false);
        if (mousePosition === null) return;

        const dx = mousePosition[0] - panStart[0];
        const dy = mousePosition[1] - panStart[1];

        if (dx === 0 && dy === 0) return;

        props.onPan?.([showX - dx, showY - dy]);
        setPanStart(mousePosition);
    }

}

export default Canvas;
