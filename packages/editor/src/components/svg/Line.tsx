/*
 * @paper-doll/editor
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import React from "react";
import { Vertex } from "@paper-doll/core";

/**
 * Line properties
 */
export interface LineProps extends Omit<React.SVGProps<SVGLineElement>, "x1" | "x2" | "y1" | "y2"> {
    /**
     * First point of line
     */
    v1: Vertex;
    /**
     * Last point of line
     */
    v2: Vertex;
}

/**
 * SVG Line
 *
 * @param props
 */
export default function Line(props: LineProps) {
    const { v1, v2, ...rest } = props;
    return <line x1={v1[0]} y1={v1[1]} x2={v2[0]} y2={v2[1]} {...rest} />;
};
