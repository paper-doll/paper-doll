/*
 * @paper-doll/editor
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import React from "react";
import { Vertex } from "@paper-doll/core";

/**
 * Polygon properties
 */
export interface PolylineProps {
    /**
     * Points of the polygon
     */
    vertices:  Vertex[];
    /**
     * Stroke of the line
     */
    stroke?: string;
    /**
     * Fill of the line area
     */
    fill?: string;
}

/**
 * SVG Polyline
 *
 * @param props
 */
const Polyline : React.FC<PolylineProps> = props => {

    const points = props.vertices
        .map(v => `${v[0]},${v[1]}`)
        .join(" ");
    return <polyline points={points} stroke={props.stroke} fill={props.fill}/>
};

export default Polyline;
