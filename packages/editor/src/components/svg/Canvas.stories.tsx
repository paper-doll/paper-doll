/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Vertex } from '@paper-doll/core';
import { action } from '@storybook/addon-actions';
import { Meta } from '@storybook/react/types-6-0';
import React from 'react';
import Canvas, { CanvasProps } from "./Canvas";

export default {
    title: "Components/svg/Canvas",
    component: Canvas,
} as Meta;

const Template = (args: CanvasProps) => {

    const [cx, setCx] = React.useState(50);
    const [cy, setCy] = React.useState(50);

    const [x, setX] = React.useState(args.showX);
    const [y, setY] = React.useState(args.showY);

    const handleElementMove = (id: string, pos: Vertex, delta: Vertex) => {
        action("onElementMove")(id, pos[0], pos[1], delta[0], delta[1]);
        setCx(cx + delta[0]);
        setCy(cy + delta[1]);
    };

    const handleElementSelect = (id: string) => action("onElementSelect")(id);

    const handlePan = (pos: Vertex) => {
        action("onPan")(pos[0], pos[1]);
        setX(pos[0]);
        setY(pos[1]);
    }

    return (
        <Canvas
            {...args}
            showX={x}
            showY={y}
            sx={{
                background: "lightGray",
                width: 400,
                height: 700
            }}

            onElementMove={handleElementMove}
            onElementSelect={handleElementSelect}
            onPan={handlePan}
        >
            <rect x={0} y={0} width="200" height="400" stroke="black" fill="white" />
            <circle data-doll-handle="myCircle" cx={cx} cy={cy} r={25} fill="red" stroke="black" />
        </Canvas>
    );
}

export const TestCanvas = Template.bind({});
TestCanvas.args = {
    canvasWidth: 200,
    canvasHeight: 400,
    zoom: 100,
    showX: 100,
    showY: 200,
};
