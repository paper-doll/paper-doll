/*
 * @paper-doll/editor
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { styled } from "@mui/material";
import { Vertex } from "@paper-doll/core";
import React from "react";
import Line from "./Line";

const StyledLine = styled(Line)(({ theme }) => ({
    stroke: theme.palette.secondary.main,
    strokeDasharray: "3,3",
}));

/**
 * Vertex point properties
 */
export interface VertexLineProps {
    /**
     * Vertex
     */
    v1: Vertex;
    /**
     * Vertex
     */
    v2: Vertex;
}

/**
 * Vertex point
 */
export default function VertexLine(props: VertexLineProps) {
    return <StyledLine v1={props.v1} v2={props.v2} />;
};
