/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Meta, Story } from '@storybook/react/types-6-0';
import React from 'react';
import VertexPoint, { VertexPointProps } from "./VertexPoint";

export default {
    title: "Components/svg/VertexPoint",
    component: VertexPoint,
} as Meta;

const Template: Story<VertexPointProps> = (args) =>
    <svg width={100} height={100}>
        <rect x={0} y={0} width={100} height={100} stroke="black" fill="white" />
        <VertexPoint {...args} />
    </svg>;

export const Simple = Template.bind({});
Simple.args = {
    id: "1",
    v: [50, 50]
};

export const SimpleSelected = Template.bind({});
SimpleSelected.args = {
    id: "1",
    v: [50, 50],
    selected: true
};

export const WithText = Template.bind({});
WithText.args = {
    id: "1",
    v: [50, 50],
    text: "50,50"
};

export const SideBySide = () =>
    <svg width={100} height={100}>
        <rect x={0} y={0} width={100} height={100} stroke="black" fill="white" />
        <VertexPoint v={[50,50]} handle="1" />
        <VertexPoint v={[51,50]} handle="2" />
    </svg>;
