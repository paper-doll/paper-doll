/*
 * @paper-doll/editor
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { styled } from "@mui/material";
import { Vertex } from "@paper-doll/core";
import React, { useState } from "react";

const Handle = styled('circle')(({ theme }) => ({
    stroke: theme.palette.primary.main,
    fill: "transparent",
}));

const Point = styled('circle')(({ theme }) => ({
    fill: theme.palette.primary.main,
    stroke: theme.palette.primary.main,
}));

const SelectedPoint = styled('circle')(({ theme }) => ({
    fill: theme.palette.secondary.main,
    stroke: theme.palette.secondary.main,
}));

const pointRadius = 1.5;
const handleRadius = 5;

/**
 * Vertex point properties
 */
export interface VertexPointProps
    extends React.DOMAttributes<SVGCircleElement> {

    /**
     * Point handle
     */
    handle: string;

    /**
     * Vertex
     */
    v: Vertex;

    /**
     * Is this selected vertex?
     */
    selected?: boolean;

    /**
     * OptionalText to show by the vertex point
     */
    text?: string;
}

/**
 * Vertex point
 */
export const VertexPoint: React.FC<VertexPointProps> = (props) => {

    const { handle, v, text, selected, ...rest } = props;

    const x = v[0];
    const y = v[1];

    const [focused, setFocus] = useState(false);

    const textView = props.text
        ? (
            <text x={x} y={y}>
                {text}
            </text>
        ) : null;

    const point = selected ?? false
        ? <Point
            {...rest}
            cx={x} cy={y} r={pointRadius}
            onMouseOver={() => setFocus(true)}
            onMouseOut={() => setFocus(false)}
        />
        : <SelectedPoint
            {...rest}
            cx={x} cy={y} r={pointRadius}
            onMouseOver={() => setFocus(true)}
            onMouseOut={() => setFocus(false)}
        />;

    const handleCircle = focused
        ? <Handle cx={x} cy={y} r={handleRadius} />
        : null;

    return (
        <g data-doll-handle={handle}>
            {handleCircle}
            {point}
            {textView}
        </g>
    );
};

export default VertexPoint;