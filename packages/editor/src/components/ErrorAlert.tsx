/*
 * @paper-doll/editor
 * Copyright © 2021 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Snackbar, Alert } from "@mui/material";
import React from "react";
import { useDispatch } from "react-redux";
import { clearError } from "../actions/clearError";
import { useEditorState } from "../model/EditorState";

export default function ErrorAlert() {

    const error = useEditorState(s => s.error);
    const isError = error !== undefined;

    const dispatch = useDispatch();

    const handleClose = () => dispatch(clearError());

    return (
        <Snackbar
            open={isError}
            anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
            onClose={handleClose}>
            <Alert onClose={handleClose} severity="error">
                {error}
            </Alert>
        </Snackbar>
    );
}