/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Stack, SxProps } from "@mui/material";
import { Vertex } from "@paper-doll/core";
import React from "react";
import IntegerEdit from "./IntegerEdit";

/**
 * VertexEdit properties
 */
export interface VertexEditProps {
    /**
     * Item value
     */
    value: Vertex;
    /**
     * Callback on value change
     */
    onValueChange?: (value: Vertex) => void;
}

/**
 * Vertex edit component
 *
 * @param props
 */
const VertexEdit: React.FC<VertexEditProps> = (props) => {

    const [x, y] = props.value;

    const handleChangeX = (value?: number) => props.onValueChange?.([value ?? 0, y]);
    const handleChangeY = (value?: number) => props.onValueChange?.([x, value ?? 0]);

    const style : SxProps = {
        width: "3.5em",
        maxWidth:"3.5em",
    };

    //<Box display="flex" flexDirection="row" flexWrap="wrap" mx={1}>

    return (
        <Stack direction="row" spacing={1}>
            <IntegerEdit
                required
                value={x}
                thousandSeparator
                onValueChange={handleChangeX}
                sx={style}
            />
            <IntegerEdit
                required
                value={y}
                thousandSeparator
                onValueChange={handleChangeY}
                sx={style}
            />
        </Stack>
    );
}

export default VertexEdit;