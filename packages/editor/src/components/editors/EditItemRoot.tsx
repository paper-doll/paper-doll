/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { styled } from "@mui/material";

export type EditItemRootProps = {
    selected: boolean;
};

export const EditItemDragHandle = styled('div')(() => ({
    margin: "1px",
    width: "2em",
    textAlign: "center",
    verticalAlign: "middle",
    userSelect: "none",
}));

export const EditItemIndex = styled('div')(() => ({
    fontSize: "0.8rem",
    textAlign: "center",
    writingMode: "vertical-lr",
}));

/**
 * Root for edit items
 */
export const EditItemRoot = styled('div')<EditItemRootProps>(({ theme, selected }) => ({
    display: "flex",
    flexWrap: "wrap",
    flexDirection: "row",
    alignItems: "center",
    border: "1px solid",
    borderColor: selected ? theme.palette.text.primary : "#FF000000",
    "&:hover": {
        background: theme.palette.grey[100]
    }
}));

export default EditItemRoot;