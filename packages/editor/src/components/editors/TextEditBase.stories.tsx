/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Meta, Story } from "@storybook/react/types-6-0";
import React from "react";
import TextEditBase, { TextEditBaseProps } from "./TextEditBase";

export default {
    title: "Components/editors/TextEditBase",
    component: TextEditBase,
} as Meta;

const Template: Story<TextEditBaseProps> = (args) =>
    <TextEditBase
        {...args}
    />;

export const Default = Template.bind({});
Default.args = {
    defaultValue: "Text"
};
