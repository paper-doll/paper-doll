import ArrayEdit from "./ArrayEdit";
import React from 'react';
import { Meta } from '@storybook/react/types-6-0';

export default {
    title: "Components/ArrayEdit",
    component: ArrayEdit,
} as Meta;

const data: Array<string> = ["first", "second"];

/**
 * Empty array
 */
export const Empty = () => {
    const data: Array<number> = [];
    return <ArrayEdit<number>
        data={data}
        selected={-1}
        onAdd={i => data.push(i)}
    />
};

export const WithData = () => {
    return <ArrayEdit<string>
        data={data}
        selected={-1}
        onAdd={i => data.splice(i, 0, `${i}th`)}
        onRemove={i => data.splice(i, 1)}
    />
}