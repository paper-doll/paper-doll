/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import React, { useEffect, useState } from "react";
import TextEditBase, { TextEditBaseProps } from "./TextEditBase";

/**
 * Properties for TreeTextEdit
 */
export interface TextEditProps extends TextEditBaseProps {
    /**
     * Value changed
     */
    onValueChange: (value: string) => void;
    /**
     * Value
     */
    value: string;
}

/**
 * Controlled text editor
 * that only triggers onChange when editor loses focus
 *
 * @param props
 * @returns
 */
export default function TextEdit(props: TextEditProps) {

    const { onValueChange, ...textProps } = props;

    const [value, setValue] = useState(props.value);

    // Update state on props value change
    useEffect(
        () => setValue(props.value),
        [props.value]
    );

    const raiseOnChange = () => {
        if (props.value !== value) {
            props.onValueChange?.(value);
        }
    }

    const handleValueChange: React.ChangeEventHandler<HTMLInputElement> =
        e => {
            setValue(e.target.value);
        };

    const handleBlur: React.FocusEventHandler<HTMLInputElement> =
        e => {
            raiseOnChange();
            props.onBlur?.(e);
        };

    const handleKeyPress: React.KeyboardEventHandler<HTMLInputElement> =
        e => {
            if (e.code === "Escape") {
                e.stopPropagation();
                setValue(props.value as string);
            };
            if (e.code === "Enter" || e.code === "NumpadEnter") {
                e.stopPropagation();
                raiseOnChange();
            }
            props.onKeyUpCapture?.(e);
        };

    return (
        <TextEditBase
            {...textProps}
            value={value}
            onChange={handleValueChange}
            onKeyUpCapture={handleKeyPress}
            onBlur={handleBlur}
        />
    );
}