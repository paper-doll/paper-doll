/*
 * @paper-doll/editor
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Box, Card, CardActions, CardContent, CardHeader, IconButton, Tooltip } from "@mui/material";
import AddIcon from '@mui/icons-material/AddCircle';
import React from "react";
import ArrayEditItem from "./ArrayEditItem";

/**
 * Array editor properties
 */
export interface ArrayEditProps<T> {
    /**
     * Array of type T to display
     */
    data: T[];

    /**
     * Optional function to render the element.
     * If not provided, value is converted to string for display purposes
     */
    render?: (value: T, index: number) => JSX.Element;

    /**
     * Selected index
     */
    selected?: number;

    /**
     * Title for the editor
     */
    title?: string;

    /**
     * Callback when different item in array is selected
     */
    onSelectedChange?: (index: number) => void;

    /**
     * Callback to add item to given index
     */
    onAdd?: (index: number) => void;

    /**
     * If true, the Add is only on whole array, not on the items
     */
    noInlineAdd?: boolean;

    /**
     * Callback to remove item at given index
     */
    onRemove?: (index: number) => void;

    /**
     * Callback to move item up
     */
    onMoveUp?: (index: number) => void;

    /**
     * Callback to move item down
     */
    onMoveDown?: (index: number) => void;
}

/**
 * Generic array editor
 */
export default function ArrayEdit<TValue>(props: ArrayEditProps<TValue>) {

    const defaultRender = (v: TValue, i: number) => <span>{`${v}`}</span>;

    const render = props.render ?? defaultRender;
    const onAdd = props.noInlineAdd
        ? undefined
        : props.onAdd;

    return (
        <Box display="flex" flexDirection="column" m={1}>
            <Card>
                <CardHeader title={props.title} />
                <CardContent>
                    {
                        props.data.map((v, i) => (
                            <ArrayEditItem
                                key={i}
                                index={i}
                                selected={i === props.selected}
                                onAdd={onAdd}
                                onRemove={props.onRemove}
                                onMoveUp={props.onMoveUp}
                                onMoveDown={props.onMoveDown}
                                onClick={props.onSelectedChange}
                            >
                                {render(v, i)}
                            </ArrayEditItem>
                        ))
                    }
                </CardContent>
                <CardActions>
                    {(props.onAdd != undefined) &&
                        <Tooltip title="add">
                            <IconButton
                                size="small"
                                color="primary"
                                onClick={() => props.onAdd?.(props.data.length)}>
                                <AddIcon />
                            </IconButton>
                        </Tooltip>
                    }
                </CardActions>
            </Card>
        </Box>
    );
}
