/*
 * @paper-doll/editor
 * Copyright © 2021 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Box } from "@mui/material";
import React from "react";
import SaveButtonBox from "./SaveButtonBox";

/**
 * Properties for item editor
 */
export interface ItemEditorProps {
    /**
     * Is editor changed?
     */
    isChanged: boolean;
    /**
     * Are data valid?
     */
    isValid: boolean;

    /**
     * Callback on cancel
     */
    onCancel: React.MouseEventHandler<HTMLButtonElement>;
    /**
     * Callback on save
     */
    onSave: React.MouseEventHandler<HTMLButtonElement>;

    /**
     * Children
     */
    children: React.ReactNode;
}

/**
 * Item editor
 * @param props
 */
export default function ItemEditor(props: ItemEditorProps) {

    return (
        <Box display="flex" flexDirection="column">
            {props.children}
            <SaveButtonBox
                show={props.isChanged}
                saveAllowed={props.isChanged && props.isValid}
                onCancel={props.onCancel}
                onSave={props.onSave}
            />
        </Box>
    );
}