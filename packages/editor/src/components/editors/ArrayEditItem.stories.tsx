import ArrayEditItem, { ArrayEditItemProps } from "./ArrayEditItem";
import React from 'react';
import { Meta, Story } from '@storybook/react/types-6-0';

export default {
    title: "Components/ArrayEditItem",
    component: ArrayEditItem,
} as Meta;

const Template: Story<ArrayEditItemProps> = (args) =>
    <ArrayEditItem {...args} >
        <span>Some value</span>
    </ArrayEditItem>;

/**
 * Simple array item with no custom render
 */
export const Primary = Template.bind({});
Primary.args = {
    index: 0,
    selected: false,
    canMove: true,
};

export const Multi = () =>
    <>
        <ArrayEditItem index={0} selected={false}>
            <span>First</span>
        </ArrayEditItem>
        <ArrayEditItem index={1} selected={true}>
            <span>Second</span>
        </ArrayEditItem>
        <ArrayEditItem index={2} selected={false}>
            <span>Third</span>
        </ArrayEditItem>
    </>;
