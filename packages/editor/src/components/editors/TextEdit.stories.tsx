/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Meta, Story } from "@storybook/react/types-6-0";
import React from "react";
import TextEdit, { TextEditProps } from "./TextEdit";

export default {
    title: "Components/editors/TextEdit",
    component: TextEdit,
} as Meta;

const Template: Story<TextEditProps> = (args) =>
    <TextEdit
        {...args}
    />;

export const Default = Template.bind({});
Default.args = {
    value: "Text"
};
