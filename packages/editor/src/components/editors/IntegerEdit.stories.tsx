/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Meta, Story } from "@storybook/react/types-6-0";
import React from "react";
import IntegerEdit, { IntegerEditProps } from "./IntegerEdit";

export default {
    title: "Components/editors/IntegerEdit",
    component: IntegerEdit,
} as Meta;

const Template: Story<IntegerEditProps> = (args) =>
    <IntegerEdit
        {...args}
    />;

export const Default = Template.bind({});
Default.args = {
    defaultValue: 123
};
