/*
 * @paper-doll/editor
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import AddIcon from '@mui/icons-material/AddCircle';
import RemoveIcon from "@mui/icons-material/HighlightOff";
import { Box, Button, ButtonGroup, IconButton, Tooltip } from "@mui/material";
import React from "react";
import EditItemRoot, { EditItemDragHandle, EditItemIndex } from './EditItemRoot';

/**
 * Array editor properties
 */
export interface ArrayEditItemProps {
    /**
     * Index of the item
     */
    index: number;

    /**
     * Children to render
     */
    children?: JSX.Element;

    /**
     * Flag to signal selected item
     */
    selected?: boolean;

    /**
     * Callback on click
     */
    onClick?: (index: number) => void;

    /**
     * Callback to add item to this index
     */
    onAdd?: (index: number) => void;

    /**
     * Callback to remove this item
     */
    onRemove?: (index: number) => void;

    /**
     * Indicates whether to show the move up/down buttons
     */
    canMove?: boolean;

    /**
     * Callback to move item up
     */
    onMoveUp?: (index: number) => void;

    /**
     * Callback to move item down
     */
    onMoveDown?: (index: number) => void;
}

/**
 * Array edit item component
 * @param props
 */
export default function ArrayEditItem(props: ArrayEditItemProps) {

    return (
        <EditItemRoot
            selected={props.selected ?? false}
            onClick={() => props.onClick?.(props.index)}
        >
            <EditItemDragHandle>
                {props.selected ? "\u25B6" : "\u22EE"}
            </EditItemDragHandle>
            <EditItemIndex>
                {props.index}
            </EditItemIndex>
            {
                (props.canMove ?? false) &&
                <ButtonGroup variant="text" orientation="vertical" size="small">
                    <Tooltip title="move up">
                        <Button color="primary" onClick={() => props.onMoveUp?.(props.index)}>
                            {"\u25B2"}
                        </Button>
                    </Tooltip>
                    <Tooltip title="move down">
                        <Button color="primary" onClick={() => props.onMoveDown?.(props.index)}>
                            {"\u25BC"}
                        </Button>
                    </Tooltip>
                </ButtonGroup >
            }
            {
                (props.onAdd != undefined) &&
                <Tooltip title="insert before">
                    <IconButton color="primary"
                        size="small"
                        onClick={() => props.onAdd?.(props.index)}>
                        <AddIcon />
                    </IconButton>
                </Tooltip>
            }
            <Box sx={{flexGrow: 2, verticalAlign: "middle"}}>
                {props.children}
            </Box>
            {
                (props.onRemove != undefined) &&
                <Tooltip title="remove">
                    <IconButton color="secondary"
                        size="small"
                        onClick={e => {
                            e.stopPropagation();
                            props.onRemove?.(props.index);
                        }}
                    >
                        <RemoveIcon />
                    </IconButton>
                </Tooltip>
            }
        </EditItemRoot>
    );
}
