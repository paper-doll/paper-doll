/*
 * @paper-doll/editor
 * Copyright © 2021 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import ColorIcon from '@mui/icons-material/FiberManualRecord';
import { Button, Dialog, DialogActions, DialogContent, SvgIcon } from "@mui/material";
import React, { useState } from "react";
import { ColorChangeHandler, SketchPicker } from "react-color";

/**
 * Color button properties
 */
export interface ColorButtonProps {
    /**
     * Selected color
     */
    color: string;
    /**
     * Size of the button
     */
    size: "small" | "medium" | "large" | undefined;
    /**
     * Value change
     */
    onChange: (newColor: string) => void;
}

/**
 * Button with color picker
 */
export default function ColorButton(props: ColorButtonProps) {

    const [open, setOpen] = useState(false);
    const [color, setColor] = useState(props.color);

    const handleOk = () => {
        setOpen(false);
        props.onChange(color);
    }

    const handleCancel = () => {
        setOpen(false);
    }

    const handleColorChange: ColorChangeHandler = c => {
        setColor(c.hex);
    }

    return (
        <>
            <SvgIcon
                onClick={() => setOpen(true)}
                fontSize={props.size ?? "inherit"}
            >
                <circle cx={12} cy={12} r={11} stroke={"#000"} fill={props.color}/>
            </SvgIcon>
            <Dialog
                open={open}
                onClose={handleCancel}
            >
                <SketchPicker
                    color={color}
                    onChangeComplete={handleColorChange}
                />
                <DialogActions>
                    <Button color="primary" onClick={handleOk}>OK</Button>
                    <Button color="error" onClick={handleCancel}>Cancel</Button>
                </DialogActions>
            </Dialog>
        </>
    );
}