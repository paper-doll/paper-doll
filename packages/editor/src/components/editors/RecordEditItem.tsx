/*
 * @paper-doll/editor
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import RemoveIcon from "@mui/icons-material/HighlightOff";
import { Box, IconButton, Tooltip } from "@mui/material";
import React from "react";
import EditItemRoot, { EditItemDragHandle } from './EditItemRoot';

/**
 * Record/Dictionary editor properties
 */
export interface RecordEditItemProps {
    /**
     * Id of the item
     */
    recordId: string;

    /**
     * Children to render
     */
    children?: JSX.Element;

    /**
     * Flag to signal selected item
     */
    selected?: boolean;

    /**
     * Callback on click
     */
    onClick?: (id: string) => void;

    /**
     * Callback to remove this item
     */
    onRemove?: (id: string) => void;
}

/**
 * Record/dictionary edit item component
 * @param props
 */
export default function RecordEditItem(props: RecordEditItemProps) {
    return (
        <EditItemRoot
            selected={props.selected ?? false}
            onClick={() => props.onClick?.(props.recordId)}
        >
            <EditItemDragHandle>
                {props.selected ? "\u25B6" : "\u22EE"}
            </EditItemDragHandle>
            <Box sx={{ flexGrow: 2, verticalAlign: "middle" }}>
                {props.children}
            </Box>
            {
                (props.onRemove != undefined) &&
                <Tooltip title="remove">
                    <IconButton color="secondary"
                        size="small"
                        onClick={e => {
                            e.stopPropagation();
                            props.onRemove?.(props.recordId);
                        }}
                    >
                        <RemoveIcon />
                    </IconButton>
                </Tooltip>
            }
        </EditItemRoot >
    );
}
