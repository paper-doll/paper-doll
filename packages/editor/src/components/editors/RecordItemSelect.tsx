/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { MenuItem, Select, SelectChangeEvent } from "@mui/material";
import React, { useState } from "react";
import { Identifiable, Nameable } from "../../model";

/**
 * Cage select properties
 */
export interface RecordItemSelectProps<TItem extends Nameable & Identifiable> {
    /**
     * Label for the select
     */
    label?: string;
    /**
     * Record with items
     */
    items: Record<string, TItem>;
    /**
     * Allow selecting "None"
     */
    allowNone? : boolean;
    /**
     * Id of the selected value
     */
    value?: string;
    /**
     * Event raised when item is selected
     */
    onSelect: (id: string | undefined) => void;
}

const NONE_VALUE = "__NONE__";

/**
 * Component for selecting one cage
 *
 * @param props
 * @returns
 */
export default function RecordItemSelect<TItem extends Nameable & Identifiable>(
    props: RecordItemSelectProps<TItem>
) {
    const source = Object.entries(props.items);

    const handleChange = (e: SelectChangeEvent) => {
        const value = e.target.value == NONE_VALUE
            ? undefined
            : e.target.value;
        props.onSelect?.(value);
    }

    const items = source.map(kv =>
        <MenuItem key={kv[0]} value={kv[0]}>{kv[1].name}</MenuItem>
    );

    if (props.allowNone) {
        items.unshift(
            <MenuItem key="none" value={NONE_VALUE}>None</MenuItem>
        );
    }

    return (
        <Select
            variant="standard"
            value={props.value ?? NONE_VALUE}
            label={props.label}
            onChange={handleChange}>
            {items}
        </Select>
    );
}