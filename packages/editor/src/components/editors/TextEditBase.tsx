import { InputUnstyled, InputUnstyledProps } from "@mui/base";
import { styled, SxProps, Theme } from "@mui/material";
import React from "react";

const StyledInputElement = styled("input")(
  ({ theme }) => `
  font-family: ${theme.typography.fontFamily};
  font-weight: 400;
  background: ${theme.palette.grey[50]};
  border: 0px;
  padding-inline: 0.25em;
  transition: all 150ms ease;

  &:hover {
    border-color: ${theme.palette.grey[400]};
  }
`
);

/**
 * Text edit props
 */
export interface TextEditBaseProps extends InputUnstyledProps {
  sx?: SxProps<Theme>;
}

/**
 * Text editor
 */
const TextEditBase = React.forwardRef(function CustomInput(
  props: TextEditBaseProps,
  ref: React.ForwardedRef<HTMLDivElement>
) {
  const { sx, ...unstyledProps } = props;

  return (
    <InputUnstyled
      components={{ Input: StyledInputElement }}
      componentsProps={{ input: { sx: props.sx } as any }}
      {...unstyledProps}
      ref={ref}
    />
  );
});

export default TextEditBase;
