/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import React from "react";
import { Button, ButtonProps } from "@mui/material";
import AddCircleIcon from '@mui/icons-material/AddCircle';

export type AddButtonProps = Omit<ButtonProps, "startIcon">;

/**
 * "Add" button
 * @param props
 * @returns
 */
export default function AddButton(props: AddButtonProps) {
    return (
        <Button
            size="small"
            color="primary"
            {...props}
            startIcon={<AddCircleIcon />}
        />
    );
}
