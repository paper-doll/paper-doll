/*
 * @paper-doll/editor
 * Copyright © 2021 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import React, { MouseEventHandler } from "react";
import { Box, IconButton } from "@mui/material";
import CheckIcon from '@mui/icons-material/Check';
import ClearIcon from '@mui/icons-material/Clear';

export interface SaveButtonBoxProps {
    /**
     * Gets whether the box should be shown
     */
    show: boolean;
    /**
     * Gets whether save is allowed
     */
    saveAllowed: boolean;
    /**
     * Callback on cancel
     */
    onCancel : MouseEventHandler<HTMLButtonElement>;
    /**
     * Callback on save
     */
    onSave: MouseEventHandler<HTMLButtonElement>;
}

/**
 * Box with Save and Cancel icons
 * @param props
 */
export default function SaveButtonBox(props: SaveButtonBoxProps) {
    return (
        <Box
            display={props.show ? "flex" : "none"}
            flexDirection="row"
            justifyContent="flex-end"
        >
            <IconButton
                onClick={props.onCancel}
                size="large"
            >
                <ClearIcon color="error" />
            </IconButton>
            <IconButton
                disabled={!props.saveAllowed}
                onClick={props.onSave}
                size="large"
            >
                <CheckIcon color={props.saveAllowed ? "primary" : "disabled"} />
            </IconButton>
        </Box>
    );
}