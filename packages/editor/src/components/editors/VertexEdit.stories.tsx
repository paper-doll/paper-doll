/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Meta, Story } from "@storybook/react/types-6-0";
import React from "react";
import VertexEdit, { VertexEditProps } from "./VertexEdit";

export default {
    title: "Components/editors/VertexEdit",
    component: VertexEdit,
} as Meta;

const Template: Story<VertexEditProps> = (args) =>
    <VertexEdit
        {...args}
    />;

export const Default = Template.bind({});
Default.args = {
    value: [567,1234]
};
