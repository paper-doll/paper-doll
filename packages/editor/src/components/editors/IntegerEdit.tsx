/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { SxProps, Theme } from "@mui/material";
import React from "react";
import NumberFormat, { NumberFormatProps, NumberFormatValues } from 'react-number-format';
import TextEditBase from "./TextEditBase";

/**
 * Integer field properties
 */
export interface IntegerEditProps extends Omit<NumberFormatProps, "onValueChange"> {
    /**
     * On change event
     */
    onValueChange: (value?: number) => void;
    /**
     * Styling prop
     */
    sx?: SxProps<Theme>;
}

/**
 * Field for editing integers
 * @param props
 */
export default function IntegerEdit(props: IntegerEditProps) {

    const {
        onValueChange,
        ...inputProps
    } = props;

    const handleChange = (values: NumberFormatValues) => {
        const int = parseInt(values.value);
        if (isNaN(int)) return;
        props.onValueChange?.(int);
    };

    return (
        <NumberFormat
            {...inputProps}
            decimalScale={0}
            customInput={TextEditBase}
            onValueChange={handleChange}
        />
    );
}
