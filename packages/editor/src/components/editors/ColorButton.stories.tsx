/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Meta, Story } from '@storybook/react/types-6-0';
import React, { useState } from 'react';
import ColorButton, { ColorButtonProps } from "./ColorButton";

export default {
    title: "Components/editors/ColorButton",
    component: ColorButton,
} as Meta;

const Template: Story<ColorButtonProps> = (args) => {

    const [color, setColor] = useState(args.color);
    return (
        <ColorButton
            {...args}
            color={color}
            onChange={c => setColor(c)}
        />
    );
}

export const InheritedSize = Template.bind({});
InheritedSize.args = {
    color: "red",
    size: undefined,
};

export const SmallSize = Template.bind({});
SmallSize.args = {
    color: "green",
    size: "small",
};
