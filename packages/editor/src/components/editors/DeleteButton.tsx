/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import React from "react";
import { Button, ButtonProps } from "@mui/material";
import DeleteIcon from '@mui/icons-material/Delete';

export type DeleteButtonProps = Omit<ButtonProps, "startIcon">;

/**
 * "Delete" button
 * @param props
 * @returns
 */
export default function DeleteButton(props: DeleteButtonProps) {
    return (
        <Button
            size="small"
            color="error"
            {...props}
            startIcon={<DeleteIcon />}
        />
    );
}
