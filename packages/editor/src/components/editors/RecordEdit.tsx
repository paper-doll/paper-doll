/*
 * @paper-doll/editor
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Box, Card, CardActions, CardContent, CardHeader, IconButton, Tooltip } from "@mui/material";
import AddIcon from '@mui/icons-material/AddCircle';
import React from "react";
import RecordEditItem from "./RecordEditItem";

/**
 * Array editor properties
 */
export interface RecordEditProps<TValue> {
    /**
     * Record of type T to display
     */
    data: Record<string | number, TValue>;

    /**
     * Optional function to render the element.
     * If not provided, value is converted to string for display purposes
     */
    render?: (value: TValue, id: string) => JSX.Element;

    /**
     * Selected id
     */
    selected?: string | number;

    /**
     * Title for the editor
     */
    title?: string;

    /**
     * Callback when different item in array is selected
     */
    onSelectedChange?: (id: string) => void;

    /**
     * Callback to add item
     */
    onAdd?: () => void;

    /**
     * Callback to remove item at given index
     */
    onRemove?: (id: string) => void;
}

/**
 * Generic array editor
 */
export default function RecordEdit<TValue>(props: RecordEditProps<TValue>) {

    const defaultRender = (v: TValue, id: string) => <span>{`${v}`}</span>;

    const render = props.render ?? defaultRender;

    return (
        <Box display="flex" flexDirection="column" m={1}>
            <Card>
                <CardHeader title={props.title} />
                <CardContent>
                    {
                        // Note the comparison for selected is using the
                        // weak equality (==) to ensure numbers compare to strings,
                        // e.g. 1 == "1" should be true
                        Object.entries<TValue>(props.data).map(e => (
                            <RecordEditItem
                                key={e[0]}
                                recordId={e[0]}
                                selected={e[0] == props.selected}
                                onRemove={props.onRemove}
                                onClick={props.onSelectedChange}
                            >
                                {render(e[1], String(e[0]))}
                            </RecordEditItem>
                        ))
                    }
                </CardContent>
                <CardActions>
                    {(props.onAdd != undefined) &&
                        <Tooltip title="add">
                            <IconButton
                                size="small"
                                color="primary"
                                onClick={props.onAdd}
                                >
                                <AddIcon />
                            </IconButton>
                        </Tooltip>
                    }
                </CardActions>
            </Card>
        </Box>
    );
}
