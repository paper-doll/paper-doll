/*
 * @paper-doll/editor
 * Copyright © 2021 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Button } from "@mui/material";
import React, { useRef } from "react";

/**
 * Properties for the download button
 */
export interface UploadButtonProps {
    /**
     * Button display
     */
    children?: React.ReactNode;

    /**
     * File types to accept.
     * If not provided, json is used
     */
    accept: string;

    /**
     * Function called on upload
     */
    onUpload: (file: File) => void;
}

/**
 * Download button component
 *
 * @param props
 */
export default function UploadButton(props: UploadButtonProps) {

    // Input
    const uploadRef = useRef<HTMLInputElement>(null);

    const upload: React.MouseEventHandler = (e) => {
        e.preventDefault();
        uploadRef?.current?.click();
    }

    const openFile = (e: React.ChangeEvent<HTMLInputElement>) => {
        if (!e.target.files) return;
        const file: File = e.target.files[0];
        props.onUpload(file);
    };

    const accept = props.accept ?? ".json,application/json";

    const display = props.children ?? "Upload";

    return (
        <>
            <Button onClick={upload}>{display}</Button>
            <input
                type="file"
                accept={accept}
                ref={uploadRef}
                style={{ display: "none" }}
                multiple={false}
                onChange={openFile}
            />
        </>
    );
}
