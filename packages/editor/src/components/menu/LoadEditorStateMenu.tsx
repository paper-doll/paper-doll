/*
 * @paper-doll/editor
 * Copyright © 2021 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import React from "react";
import UploadButton from "./UploadButton";
import LoadIcon from "@mui/icons-material/FolderOpen";
import { Tooltip } from "@mui/material";
import { useDispatch } from "react-redux";
import { loadEditorState } from "../../actions/doll/LoadEditorStateAction";

/**
 * Menu button to load editor state from file
 */
export default function LoadEditorStateMenu() {

    const dispatch = useDispatch();

    const onLoaded = (e: ProgressEvent<FileReader>) => {
        if (!e.target) return;
        const rawData = e.target.result as string;
        const data = JSON.parse(rawData);
        dispatch(loadEditorState(data));
    };

    const readFile = (f: File) => {
        const reader = new FileReader();
        reader.onload = onLoaded
        reader.readAsText(f);
    }

    return (
        <UploadButton
            accept="*.json,application/json"
            onUpload={readFile}
        >
            <Tooltip title={"Load data"} placement="right" arrow>
                <LoadIcon sx={{color: "primary.contrastText"}}/>
            </Tooltip>
        </UploadButton>
    );
}
