/*
 * @paper-doll/editor
 * Copyright © 2021 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Tooltip } from "@mui/material";
import SaveIcon from "@mui/icons-material/Save";
import React from "react";
import { useEditorState } from "../../model/EditorState";
import DownloadButton from "./DownloadButton";

/**
 * Menu to save editor state to file
 */
export default function SaveEditorStateMenu() {

    const state = useEditorState(s => s);

    const saveState = () => {
        const output = JSON.stringify(state);
        const blob = new Blob([output], { type: "application/json" });
        return blob;
    };

    return (
        <DownloadButton
            filename="doll"
            dataProvider={saveState}
        >
            <Tooltip title={"Save data"} placement="right" arrow>
                <SaveIcon sx={{color: "primary.contrastText"}}/>
            </Tooltip>
        </DownloadButton>
    );
}