/**
 * @paper-doll/editor
 * Copyright © 2020 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import WidgetsOutlinedIcon from '@mui/icons-material/WidgetsOutlined';
import { DropzoneDialog } from "mui-file-dropzone";
import React from "react";
import { useDispatch } from "react-redux";
import { importMesh } from "../../actions/mesh/ImportMeshAction";
import MenuButton from './MenuButton';

/**
 * Menu item for importing SVG files as mesh
 *
 */
export const ImportMeshSvgMenuItem: React.FC = () => {

    const [open, setOpen] = React.useState(false);
    const dispatch = useDispatch();

    const handleOpen = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
        setOpen(true);
    }

    const handleClose = () => {
        setOpen(false);
    }

    const handleFiles = (files: File[]) => {
        handleClose();
        if (!files) return;

        const file = files[0];
        const reader = new FileReader();
        reader.onload = () => {
            const svgText = reader.result as string;
            dispatch(importMesh(svgText));
        };
        reader.readAsText(file);
    }

    return (
        <>
            <DropzoneDialog
                acceptedFiles={["image/svg+xml"]}
                fileObjects={undefined}
                filesLimit={1}
                open={open}
                onClose={handleClose}
                onSave={handleFiles}
            />
            <MenuButton
                title="Import SVG Mesh"
                onClick={handleOpen}>
                <WidgetsOutlinedIcon />
            </MenuButton>
        </>
    );
}

export default ImportMeshSvgMenuItem;
