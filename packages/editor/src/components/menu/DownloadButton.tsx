/*
 * @paper-doll/editor
 * Copyright © 2021 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { Button } from "@mui/material";
import React, { useEffect, useRef, useState } from "react";

/**
 * Properties for the download button
 */
export interface DownloadButtonProps {
    /**
     * Button display
     */
    children?: React.ReactNode;

    /**
     * Name of the file
     */
    filename: string;

    /**
     * Data to download
     */
    data?: any;

    /**
     * Mime type for the data.
     * If not provided, "application/json" will be used
     */
    mime?: string;

    /**
     * Function to get raw data blob to download.
     * Either this or the data must be provided
     */
    dataProvider?: () => Blob;
}

/**
 * Download button component
 *
 * @param props
 */
export default function DownloadButton(props: DownloadButtonProps) {

    // Download url
    const [url, setUrl] = useState<string>("");
    const linkRef = useRef<HTMLAnchorElement>(null);

    const download: React.MouseEventHandler = (e) => {
        e.preventDefault();
        let blob: Blob;
        if (props.dataProvider) {
            blob = props.dataProvider();
        }
        else {
            const output = JSON.stringify(props.data, null, 2);
            blob = new Blob([output], { type: props.mime ?? "application/json" });
        }
        const url = URL.createObjectURL(blob);
        setUrl(url);
    };

    useEffect(
        () => {
            if (url !== "") {
                linkRef?.current?.click();
            };
        },
        [url]
    );

    const display = props.children ?? "Download";

    return (
        <>
            <Button onClick={download}>{display}</Button>
            <a style={{display: "none"}}
                download={props.filename}
                href={url}
                ref={linkRef}
                onClick={() => { setUrl("") }}
            >
                download
            </a>
        </>
    );
}