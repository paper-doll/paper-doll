import DownloadButton, { DownloadButtonProps } from "./DownloadButton";
import React from 'react';
import { Meta, Story } from '@storybook/react/types-6-0';
import IconButton from "@mui/material/IconButton";
import SaveIcon from "@mui/icons-material/Save";

export default {
    title: "Components/DownloadButton",
    component: DownloadButton,
} as Meta;

const Template: Story<DownloadButtonProps> = (args) => <DownloadButton {...args} />

const data = { a: 1, b: 2 };

/**
 * Simple download button with text
 */
export const Text = Template.bind({});
Text.args = {
    children: "Down",
    filename: "test.json",
    data
};

/**
 * Simple download button with icon
 */
export const Icon = () => (
    <DownloadButton data={data} filename="test.json">
        <IconButton size="large">
            <SaveIcon />
        </IconButton>
    </DownloadButton>
);

/**
 * Simple download button with icon
 */
export const IconAndText = () => (
    <DownloadButton data={data} filename="test.json">
        <IconButton size="large">
            <SaveIcon />
        </IconButton>
        Save
    </DownloadButton>
);
