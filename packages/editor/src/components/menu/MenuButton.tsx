import { IconButton, Tooltip } from "@mui/material";
import React from "react";

/**
 * Menu button properties
 */
export interface MenuButtonProps {
    /**
     * Title to show on tooltip
     */
    title: string;
    /**
     * Callback on click
     */
    onClick: React.MouseEventHandler<HTMLButtonElement>;
    /**
     * Children - the icon
     */
    children: React.ReactChild;
}

/**
 * Main Menu button
 * @param props
 */
export const MenuButton: React.FC<MenuButtonProps> = (props: MenuButtonProps) => {
    return (
     <Tooltip title={props.title} placement="right" arrow>
         <IconButton onClick={props.onClick} size="large">
             {props.children}
         </IconButton>
     </Tooltip>
    );
};

export default MenuButton;
