/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { AppBar, CssBaseline, Grid, Toolbar, Typography } from "@mui/material";
import { useWindowHeight } from "@react-hook/window-size/throttled";
import React from "react";
import { useDispatch } from "react-redux";
import { MeshSelection } from "../actions/parts";
import { select } from "../actions/Select";
import { Selection, selectionToString, useEditorState } from "../model";
import CageEdit from "./cage/CageEdit";
import CageFaceEdit from "./cage/CageFaceEdit";
import CagesContainerEdit from "./doll/CagesContainerEdit";
import DollCanvas from './doll/DollCanvas';
import DollEdit from './doll/DollEdit';
import DollTree from "./doll/DollTree";
import MorphsContainerEdit from "./doll/MorphsContainerEdit";
import PartsContainerEdit from "./doll/PartsContainerEdit";
import ErrorAlert from "./ErrorAlert";
import LoadEditorStateMenu from "./menu/LoadEditorStateMenu";
import SaveEditorStateMenu from "./menu/SaveEditorStateMenu";
import MeshEdit from "./parts/MeshEdit";
import PartEdit from "./parts/PartEdit";

function getEditor(selection: Selection) {
    switch (selection.type) {
        case "C":
            if (selection.cageId === undefined) return <CagesContainerEdit />;
            if (selection.faceId === undefined) return <CageEdit cageId={selection.cageId} />
            return (
                <CageFaceEdit
                    cageId={selection.cageId}
                    faceId={selection.faceId}
                    vertexId={selection.vertexId ?? 0}
                />
            );

        case "D":
            return <DollEdit />;

        case "M":
            if (selection.morphId === undefined) return <MorphsContainerEdit />;
            return null;

        case "P":
            if (selection.partId === undefined) return <PartsContainerEdit />;
            if (selection.placement === undefined) return <PartEdit partId={selection.partId} />;
            if (selection.meshId == undefined)return <PartEdit partId={selection.partId} />; // TODO: F/B edit
            return (
                <MeshEdit mesh={selection as MeshSelection} />
            );
    }
}

/**
 * Main editor component
 *
 * Use children to specify how the game should be rendered.
 * If no child will be specified, the CurrentStage component will be used.
 */
export const EditorApp: React.FC = props => {

    const dispatch = useDispatch();

    const doll = useEditorState(s => s.doll);
    const dollName = useEditorState(s => s.doll.name);

    const selection = useEditorState(s => s.selection);
    const selectionString = selectionToString(selection);

    // Window size
    const windowHeight = useWindowHeight();

    const editor = getEditor(selection);

    return (
        <React.Fragment>
            <CssBaseline />
            <AppBar position="static">
                <Toolbar>
                    <SaveEditorStateMenu />
                    <Typography variant="h6" sx={{ flexGrow: 1 }}>
                        Paper Doll Editor - {dollName}
                    </Typography>
                    <LoadEditorStateMenu />
                </Toolbar>
            </AppBar>
            <Grid container spacing={1} justifyContent="center" alignItems="flex-start" marginTop={1}>
                <Grid item xs={1}>
                    /* MENU BUTTONS GO HERE*/
                </Grid>
                <Grid item xs={2}>
                    <DollTree
                        doll={doll}
                        maxHeight={windowHeight * 0.75}
                        onSelect={p => dispatch(select(p))}
                    />
                </Grid>
                <Grid item xs={1}>
                    /* CANVAS BUTTONS GO HERE*/
                </Grid>
                <Grid item xs={4}>
                    <DollCanvas
                        doll={doll}
                        windowHeight={windowHeight}
                        selection={selectionString}
                    />
                </Grid>
                <Grid item xs={3}>
                    {editor}
                </Grid>
            </Grid>
            <ErrorAlert />
        </React.Fragment>
    );
};

export default EditorApp;