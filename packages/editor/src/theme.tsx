/*
 * @paper-doll/editor
 * Copyright © 2022 Galeanne Thorn
 * MIT License - http://www.opensource.org/licenses/MIT
 */

import { createTheme, responsiveFontSizes } from "@mui/material";
import "@fontsource/nunito-sans";

export const Theme = responsiveFontSizes(
    createTheme({
        typography: {
            fontSize: 13, // MUI Default is 14
            fontFamily: "Nunito Sans",
        },
        palette: {
            primary: {
                main: "#1a237e",
                contrastText: "#ffffff",
            },
            secondary: {
                main: "#b0bec5",
                contrastText: "#000000",
            },
            background: {
                default: "#fff8ef",
            },
        },
    })
);

export default Theme;