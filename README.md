# PAPER Doll

## Packages:

* Core - Structures
        * Cage
        * Modifier
        * Graphics
        * Doll
        * Doll Piece

* Binder - binds graphics to cage

* Transformer - Transforms bound graphics according to cage

* Renderer - Component to render the doll in html pages

* Editor - UI for editing

* Demo - Demo of Renderer
